<?php

require_once dirname(__FILE__).'/mock/functions.php';
require_once dirname(__FILE__).'/../programs/caldav.class.php';
require_once dirname(__FILE__).'/mock/calendar.php';





class LibCaldav_attendeeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var LibCaldav_mock
     */
    static $mock;

    public static function setUpBeforeClass()
    {
        self::$mock = bab_getInstance('LibCaldav_mock');
    }
    
    public function testOviWithCaldavAttendeeSaveEvent()
    {
        $oviUser = self::$mock->getOviUser();
        $caldavUser = self::$mock->getCaldavUser();
        $oviBackend = self::$mock->getOviBackend();
        $caldavBackend = self::$mock->getCaldavBackend();
        
        
        $oviCalendar = $oviBackend->PersonalCalendar($oviUser);
        $collection = $oviBackend->CalendarEventCollection($oviCalendar);
        
        $caldavCalendar = $caldavBackend->PersonalCalendar($caldavUser);
        
        $dtstart = BAB_DateTime::fromIsoDateTime('2015-01-01 00:00:00');
        $dtend = BAB_DateTime::fromIsoDateTime('2015-01-02 00:00:00');
        
        $period = new bab_CalendarPeriod();
        $period->setCollection($collection);
        $period->setDates($dtstart, $dtend);
        $period->setProperty('UID', 'LIBCALDAV_UNITTEST_2');
        $period->setProperty('SUMMARY', 'Test event');
        $period->setProperty('STATUS', 'CONFIMED');
        
        $period->addAttendee($oviCalendar);
        $period->addAttendee($caldavCalendar);
        
        $collection->addPeriod($period);
        
        $this->assertEquals(2, count($period->getAttendees()), 'verify number of attendees');
        
        $this->assertTrue($oviBackend->savePeriod($period));
        
        // create association with external calendars:
        $period->commitEvent();
    }
    
    
    
    
    public function testOviGetEvent()
    {
        $oviUser = self::$mock->getOviUser();
        $oviBackend = self::$mock->getOviBackend();
        
        $calendar = $oviBackend->PersonalCalendar($oviUser);
        $collection = $oviBackend->CalendarEventCollection($calendar);
        
        $period = $oviBackend->getPeriod($collection, 'LIBCALDAV_UNITTEST_2');
        
        $this->assertInstanceOf('bab_CalendarPeriod', $period);
        $this->assertEquals('Test event', $period->getProperty('SUMMARY'));
    }
    
    
    protected function getPeriod()
    {
        $caldavUser = self::$mock->getCaldavUser();
        $caldavBackend = self::$mock->getCaldavBackend();
        
        $calendar = $caldavBackend->PersonalCalendar($caldavUser);
        $collection = $caldavBackend->CalendarEventCollection($calendar);
        
        return $caldavBackend->getPeriod($collection, 'LIBCALDAV_UNITTEST_2');
    }
    
    
    protected function getOviPeriod()
    {
        $oviUser = self::$mock->getOviUser();
        $oviBackend = self::$mock->getOviBackend();
    
        $calendar = $oviBackend->PersonalCalendar($oviUser);
        $collection = $oviBackend->CalendarEventCollection($calendar);
    
        return $oviBackend->getPeriod($collection, 'LIBCALDAV_UNITTEST_2');
    }
    
    
    public function testCaldavGetEvent()
    {
        $period = $this->getPeriod();
    
        $this->assertInstanceOf('bab_CalendarPeriod', $period);
        $this->assertEquals('Test event', $period->getProperty('SUMMARY'));
    }
    
    public function testCaldavSearchEvent()
    {
        $caldavUser = self::$mock->getCaldavUser();
        $caldavBackend = self::$mock->getCaldavBackend();
        
        $calendar = $caldavBackend->PersonalCalendar($caldavUser);
        
        $from = BAB_DateTime::fromIsoDateTime('2015-01-01 00:00:00');
        $to = BAB_DateTime::fromIsoDateTime('2015-01-02 00:00:00');
        
        
        $query = $caldavBackend->Criteria();
        
        $criteria = $query->Begin($from)
            ->_AND_($query->End($to))
            ->_AND_($query->Calendar($calendar));
        
        $arr = $caldavBackend->selectPeriods($criteria);
        
        // can be 2 if caldav calendar has net been reset
        //$this->assertEquals(1, count($arr));
        
        $period = $arr[0];
        
        $this->assertInstanceOf('bab_CalendarPeriod', $period);
        $this->assertEquals('Test event', $period->getProperty('SUMMARY'));
    }
    
    
    public function testCancelInAllCalendars()
    {
        $caldavUser = self::$mock->getCaldavUser();
        $caldavBackend = self::$mock->getCaldavBackend();
        
        $calendar = $caldavBackend->PersonalCalendar($caldavUser);
        
        $period = $this->getPeriod();
        $this->assertTrue($period->cancelFromAllCalendars($calendar));
        
        $this->assertEquals('CANCELLED', $this->getPeriod()->getProperty('STATUS'));
        
        $oviPeriod = $this->getOviPeriod();
        $this->assertEquals('CANCELLED', $oviPeriod->getProperty('STATUS'));
    }
    
    
    public function testDeleteEvent()
    {
        $period = $this->getPeriod();
        $period->delete();

        $deletedPeriod = $this->getPeriod();
        
        $this->assertEquals(null, $deletedPeriod);
        
        // event is not deleted in ovidentia backend!
    }
}
