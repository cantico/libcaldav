#!/bin/bash

# Setup script to use on drone.io

sudo apt-get install radicale

sudo radicale &

mysql -u root -e 'create database test;'
mysql -u root -e 'GRANT USAGE ON test.* TO "test"@"%";'

composer install
