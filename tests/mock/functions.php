<?php 

ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_STRICT);


$babInstallPath = 'vendor/ovidentia/ovidentia/ovidentia/';
$GLOBALS['babInstallPath'] = $babInstallPath;

require_once $GLOBALS['babInstallPath'].'utilit/dbutil.php';
require_once $GLOBALS['babInstallPath'].'utilit/defines.php';
require_once $GLOBALS['babInstallPath'].'utilit/addonapi.php';
require_once $GLOBALS['babInstallPath'].'utilit/userincl.php';
require_once $GLOBALS['babInstallPath'].'utilit/utilit.php';

$GLOBALS['babDBHost'] = 'localhost';
$GLOBALS['babDBLogin'] = 'test';
$GLOBALS['babDBPasswd'] = '';
$GLOBALS['babDBName'] = 'test';

$babDB = new babDatabase();
$GLOBALS['babDB'] = $babDB; // globalize

exec('mysql -u test test < vendor/ovidentia/ovidentia/install/babinstall.sql');
exec('mysql -u test test < tests/mock/server.sql');
exec('mysql -u test test < tests/mock/ovidentia.sql');

// overwrite server host by environement variable
// export RADICALE_URL="http://localhost:5232/"

if ($new_url = getenv('RADICALE_URL')) {
    $babDB->db_query('UPDATE libcaldav_servers SET server_url='.$babDB->quote($new_url));
}


require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
require_once $GLOBALS['babInstallPath'].'utilit/cal.backend.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
$session = bab_getInstance('bab_Session');
/*@var $session bab_Session */
$session->setStorage(new bab_SessionMockStorage());

// functionality registration

$functionalities = new bab_functionalities();
$functionalities->register('CalendarBackend', 'vendor/ovidentia/ovidentia/ovidentia/utilit/cal.backend.class.php');
$functionalities->register('CalendarBackend/Ovi', 'vendor/ovidentia/ovidentia/ovidentia/utilit/cal.backend.ovi.class.php');
$functionalities->register('CalendarBackend/Caldav', 'programs/caldav.class.php');

// includes needed for tests

require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
require_once $GLOBALS['babInstallPath'].'utilit/cal.calendarperiod.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/calapi.php';