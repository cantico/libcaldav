<?php 





class LibCaldav_mock
{
    /**
     * @var Func_CalendarBackend_Ovi
     */
    private $oviBackend;
    
    /**
     * @var Func_CalendarBackend_Caldav
     */
    private $caldavBackend;
    
    
    /**
     * @var int
     */
    private $oviUser;
    
    /**
     * @var int
     */
    private $caldavUser;
    
    
    /**
     * @var Func_CalendarBackend_Ovi
     */
    public function getOviBackend()
    {
        if (!isset($this->oviBackend)) {
            $this->oviBackend = new Func_CalendarBackend_Ovi();
        }
        
        return $this->oviBackend;
    }
    
    
    
    /**
     * @return Func_CalendarBackend_Caldav
     */
    public function getCaldavBackend()
    {
        if (!isset($this->caldavBackend)) {
            $this->caldavBackend = new Func_CalendarBackend_Caldav();
        }
    
        return $this->caldavBackend;
    }
    
    /**
     * @return int
     */
    public function getOviUser()
    {
        if (!isset($this->oviUser)) {
            $this->oviUser = bab_addUser('ovi', 'ovi', '', 'ovi@example.com', 'ovi', 'secret', 'secret', true, $error);
        }
        
        return $this->oviUser;
    }
    
    
    /**
     * Create a user in ovidentia
     * and link to a caldav backend
     *
     * @return int
     */
    public function getCaldavUser()
    {
        if (!isset($this->caldavUser)) {
    
            $this->caldavUser = false;
    
            $id_user = bab_addUser('caldav', 'caldav', '', 'caldav@example.com', 'caldav', 'secret', 'secret', true, $error);

            if (!$id_user) {
                throw new Exception($error);
            }

            $backend = $this->getCaldavBackend();
    
            bab_setPersonnalCalendarBackend($id_user, $backend);
    
            $backend->setUserIdentifier($id_user, 'caldav_login');
            $backend->setUserPassword($id_user, ''); // radicale do not need password
            $backend->setServerId($id_user, 1); // default server ID from the mock/server.sql file
    
            $this->caldavUser = $id_user;
        }
        return $this->caldavUser;
    }
    
}