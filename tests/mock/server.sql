
DROP TABLE IF EXISTS `libcaldav_servers`;

CREATE TABLE `libcaldav_servers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  `server_url` varchar(255) NOT NULL DEFAULT '',
  `user_calendar_path` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
);

INSERT INTO `libcaldav_servers` VALUES (1,'Radicale','http://localhost:5232/','%u/calendar.ics/');
