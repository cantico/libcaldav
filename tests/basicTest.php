<?php

require_once dirname(__FILE__).'/mock/functions.php';
require_once dirname(__FILE__).'/../programs/caldav.class.php';
require_once dirname(__FILE__).'/mock/calendar.php';





class LibCaldav_basicTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var LibCaldav_mock
     */
    static $mock;

    public static function setUpBeforeClass()
    {
        self::$mock = bab_getInstance('LibCaldav_mock');
    }
    
    
    
    public function testStorageBackendValue()
    {
        $this->assertTrue(self::$mock->getCaldavBackend()->StorageBackend());
    }
    
    
    
    public function testServer()
    {
        $id_user = self::$mock->getCaldavUser();
        $backend = self::$mock->getCaldavBackend();
        $this->assertTrue($backend->checkCalendar($id_user), 'check calendar on localhost:5232');
    }
    
    
    public function testEventSave()
    {
        $id_user = self::$mock->getCaldavUser();
        $backend = self::$mock->getCaldavBackend();
        
        
        $calendar = $backend->PersonalCalendar($id_user);
        $collection = $backend->CalendarEventCollection($calendar);
        
        $dtstart = BAB_DateTime::fromIsoDateTime('2015-01-01 00:00:00');
        $dtend = BAB_DateTime::fromIsoDateTime('2015-01-02 00:00:00');
        
        $period = new bab_CalendarPeriod();
        $period->setDates($dtstart, $dtend);
        $period->setProperty('UID', 'LIBCALDAV_UNITTEST_1');
        $period->setProperty('SUMMARY', 'Test event');
        
        $collection->addPeriod($period);
        
        $this->assertTrue($backend->savePeriod($period));
    }
    
    
    public function testEventGet()
    {
        $id_user = self::$mock->getCaldavUser();
        $backend = self::$mock->getCaldavBackend();
        $calendar = $backend->PersonalCalendar($id_user);
        $collection = $backend->CalendarEventCollection($calendar);
        
        $period = $backend->getPeriod($collection, 'LIBCALDAV_UNITTEST_1');
        
        $this->assertInstanceOf('caldav_CalendarPeriod', $period);
        $this->assertEquals('Test event', $period->getProperty('SUMMARY'));
    }
}
