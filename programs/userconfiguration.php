<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
require_once dirname(__FILE__) . '/functions.php';



function caldav_editUserConfiguration($userId)
{
	global $babBody, $babDB;

	$babBody->setTitle(caldav_translate('CalDAV personal calendar configuration'));

	/* @var $caldav Func_CalendarBackend_Caldav */
	$caldav = bab_functionality::get('CalendarBackend/Caldav');

	if (!$caldav) {
		$babBody->addError(caldav_translate('Unable to instanciate "CalendarBackend/Caldav" functionality.'));
		$babBody->addError(caldav_translate('Please check that the LibCaldav addon is correctly installed.'));
		return;
	}

	if (!bab_isUserAdministrator() || empty($userId)) {
		$userId = $GLOBALS['BAB_SESS_USERID'];
	}

	$userIdentifier =  $caldav->getUserIdentifier($userId);
	$userPassword = $caldav->getUserPassword($userId);
	$useruniqueid = $caldav->getUniqueId($userId);
	
	$srv = $babDB->db_query('SELECT id, name,use_unique_id FROM libcaldav_servers ORDER BY name');

	$csrfField = '';
	if (class_exists('bab_CsrfProtect')) {
    	$csrf = bab_getInstance('bab_CsrfProtect');
    	/*@var $csrf bab_CsrfProtect */
	
	    $csrfField = '<input type="hidden" name="babCsrfProtect" value="'.bab_toHtml($csrf->getToken()).'" />';
	}
	
	$form = '<form name="spaces_configuration" method="post" action="' . $GLOBALS['babPhpSelf'] . '">
	    '.$csrfField.'
 		<input type="hidden" name="tg" value="' . bab_toHtml(bab_rp('tg')). '">
 		<input type="hidden" name="idx" value="saveConfiguration">
 		<input type="hidden" name="user" value="' . bab_toHtml($userId) . '">

  		<table width="90%" border="0" cellspacing="0" cellpadding="2" align="center">
		  <tr>
		   <td class="BabLoginCadreBackground" align="center" valign="middle">
		   <table class="BabLoginMenuBackground"  width="100%" border="0" cellspacing="0" cellpadding="6" align="center">
		    <tr>
		     <td align="left" colspan="2"><b>' . sprintf(caldav_translate('CalDav account for %s'), bab_getUserName($userId, true)) . '</b></td>
		    </tr>
		    <tr>
		     <td align="right">' . caldav_translate('Server') . '</td>
		     <td align="left">
		     	<select id=selecter name="server">
		     	';
	
	$selected_server = $caldav->getUserServerId($userId);
	while($arr = $babDB->db_fetch_assoc($srv))
	{
		$form .= 	'<option';
		if($arr['use_unique_id']== "true")
		{
		    $form .= " id=need ";
		}
		else
		{
		    $form .= " id=dontneed ";
		}
        $form .= 'value="'.bab_toHtml($arr['id']).'"';
		if ($selected_server == $arr['id'])
		{
			$form .= ' selected="selected"';
		}
		$form .= 	'>'.bab_toHtml($arr['name']).'</option>';
	}
	$form .= 	'</select>
		     </td>
		    </tr>
		    <tr>
		     <td align="right">' . caldav_translate('Caldav user identifier') . '</td>
		     <td align="left"><input type="text" name="user_identifier" size="20" value="' . bab_toHtml($userIdentifier) . '"></td>
		    </tr>
		    <tr>
		     <td align="right">' . caldav_translate('Caldav user password') . '</td>
		     <td align="left"><input type="password" name="user_password" size="20" value="' . bab_toHtml($userPassword) . '"></td>
		    </tr>
            <tr id="demandeid2" display:none>
                <td colspan="3" align="center">'.caldav_translate("This server need your unique id.").'</td>
            </tr>
            <tr id="demandeid" style="display:none;"';
                    
                    $form .= '>
                <td align="right">'.caldav_translate('Caldav unique identifier') . '</td>
                <td align="left"><input type="text" name="unique_id" size="20" value="'.bab_toHtml($useruniqueid).'"></td>    
            </tr>
            <tr id="demandeid3" display:none>
                <td colspan="3" align="center">'.caldav_translate("This id can be found in the Url, example: https://url.yourcaldavserver.com/xxxxx/xxxxx").'</td>
            </tr>
            <tr>
		     <td colspan="2" align="center"><input type="submit" value="' . caldav_translate('Save') . '" /></td>
		    </tr>
		   </table>
		   </td>
		  </tr>
		 </table>
		</form>';
	
	$form .= "\n <br><script type=text/javascript> $(document).ready(function(){
		if($('option:selected',$('#selecter')).attr('id')=='need')
               {
                $('#demandeid').show();
                $('#demandeid2').show();
                $('#demandeid3').show();

            }
            else
            {
                $('#demandeid').hide();
                $('#demandeid2').hide();
                $('#demandeid3').hide();
            }
        $('#selecter').change(function(){
            if($('option:selected',this).attr('id')=='need')
               {
                $('#demandeid').show();
                $('#demandeid2').show();
                $('#demandeid3').show();
            }
            else
            {
                $('#demandeid').hide();
                $('#demandeid2').hide();
                $('#demandeid3').hide();
            }
        });
    })</script>
";

	$babBody->babPopup($form);
}



function caldav_saveUserConfiguration($userId, $serverId, $userIdentifier, $userPassword,$useuniqueid)
{
	global $babBody;

	/* @var $caldav Func_CalendarBackend_Caldav */
	$caldav = bab_functionality::get('CalendarBackend/Caldav');

	if (!$caldav) {
		$babBody->addError(caldav_translate('Unable to instanciate \'CalendarBackend/Caldav\' functionality.'));
		$babBody->addError(caldav_translate('Please check that the LibCaldav addon is correctly installed.'));
		return false;
	}

	if (!bab_isUserAdministrator() || empty($userId)) {
		$userId = $GLOBALS['BAB_SESS_USERID'];
	}


	// Test connection to caldav server.
	$server = $caldav->getServer($serverId);
	$caldav->setServerId($userId, $serverId);
	$caldav->setUserIdentifier($userId, $userIdentifier);
	$caldav->setUserPassword($userId, $userPassword);
    $caldav->setUniqueId($userId, $useuniqueid);
	$user_calendar_path = $caldav->processCalendarPathTemplate($server['user_calendar_path'], $userIdentifier, $userId);
	$path = $server['server_url'] . $user_calendar_path;
	
	if (substr($path, -1, 1) !== '/') {
		$path .= '/';
	}

	$caldavClient = new CalDAVClient($path, $userIdentifier, $userPassword, null);

	$xml = '<?xml version="1.0" encoding="utf-8" ?>
	<D:propfind xmlns:D="DAV:">
	<D:prop>
	<D:current-user-privilege-set/>
	</D:prop>
	</D:propfind>';

	try {
		$caldavClient->SetDepth(0);
		$response = $caldavClient->DoXMLRequest('PROPFIND', $xml, '');
	} catch(Exception $e) {
		bab_debug($e->getMessage());
	}


	if (empty($response))
	{
		// no response, bad calendar or bad password
		$babBody->addError(caldav_translate('This account does not exists or the password is invalid'));
		return false;
	}

	bab_debug($response);
	$babBody->babEcho('<div style="border: 1px solid #cca; padding: 1em; margin: 2em; background-color: #eec">' . caldav_translate('Configuration saved.') . '</div>');

	return true;
}






$idx = bab_rp('idx');

switch($idx) {

	case 'saveConfiguration':
		caldav_saveUserConfiguration(bab_rp('user'), bab_rp('server'), bab_rp('user_identifier'), bab_rp('user_password'), bab_rp('unique_id'));
		caldav_editUserConfiguration(bab_rp('user'));
		break;

	default:
	case 'configuration':
		caldav_editUserConfiguration(bab_rp('user'));
		break;
}
