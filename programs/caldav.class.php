<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2010 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/caldav/caldav-client.php';
require_once dirname(__FILE__) . '/sessionfile.class.php';

bab_functionality::includefile('CalendarBackend');


class Func_CalendarBackend_Caldav extends Func_CalendarBackend
{


	/**
	 * @var CalDAVClient
	 */
	protected $caldavClient;


	/**
	 * @var string
	 */
	private $serverUrl = null;




	/**
	 * @return string
	 * @static
	 */
	public function getDescription()
	{
		return caldav_translate('CalDAV server');
	}


	/**
	 * The backend can be used as a storage backend for the existing calendars (personal only for now)
	 * @return bool
	 */
	public function StorageBackend()
	{
		return $this->getStorageBackend();
	}


	/**
	 * Register myself as a functionality.
	 * @static
	 */
	public function register()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
		$functionalities = new bab_functionalities();
		$functionalities->registerClass(__CLASS__, __FILE__);
	}




	public function includeEventCalendar()
	{
		require_once dirname(__FILE__).'/eventcalendar.class.php';
	}




	/**
	 *
	 * @param	int	$userId		owner of calendar
	 *
	 * @return caldav_PersonalCalendar
	 */
	public function PersonalCalendar($userId)
	{
		$this->includeEventCalendar();
		$calendar = new caldav_PersonalCalendar($this);
		$calendar->setIdUser($userId);
		return $calendar;
	}

	
	
	public function ResourceCalendar($id, Array $configuration)
	{
		$this->includeEventCalendar();
		$calendar = new caldav_ResourceCalendar($this);
		$calendar->setUid($id);
		$calendar->setConfiguration($configuration);
		return $calendar;
	}
	



	public function includeCalendarPeriod()
	{
		require_once dirname(__FILE__).'/calendarperiod.class.php';
	}




	/**
	 * Create new calendar period
	 *
	 * @param int $begin	Timestamp
	 * @param int $end		Timestamp
	 *
	 * @return caldav_CalendarPeriod
	 */
	public function CalendarPeriod($begin = null, $end = null)
	{
		$this->includeCalendarPeriod();
		return new caldav_CalendarPeriod();
	}



	public function includeCalendarAlarm()
	{
		require_once dirname(__FILE__).'/calendarperiod.class.php';
	}

	/**
	 * Create new calendar alarm
	 * VALARM object item, store rules for reminder on event
	 * @see bab_CalendarPeriod::setAlarm()
	 *
	 * @return caldav_CalendarAlarm
	 */
	public function CalendarAlarm()
	{
		$this->includeCalendarAlarm();
		return new caldav_CalendarAlarm();
	}



	public function getProdId()
	{
		return 'PRODID:-//Cantico//NONSGML Ovidentia CalDAV Client//EN';
	}


	/**
	 * Returns the timezone name. E.g. 'Europe/Paris'.
	 *
	 * @return string
	 */
	static public function getTimezoneName()
	{
		$timezone = date_default_timezone_get();
	    if (strtolower($timezone) === 'system/localtime') {
	    	$timezone = 'Europe/Paris';
	    }

	    return $timezone;
	}


	/**
	 * Returns the timezone's icalendar definition.
	 *  BEGIN:VTIMEZONE
	 *  ...
	 *  END:VTIMEZONE
	 *
	 * @return string
	 */
	public function getTimeZone()
	{
	    static $vtimezone = null;
	    if (isset($vtimezone)) {
	        return $vtimezone;
	    }

	    $timezone = self::getTimezoneName();

	    if ($addon = bab_getAddonInfosInstance('LibCaldav')) {
	        $timezoneFilename = $addon->getTemplatePath() . 'zoneinfo/' . $timezone . '.ics';
	    } else {
	        $timezoneFilename = dirname(__FILE__).'/../skins/ovidentia/templates/zoneinfo/' . $timezone . '.ics';
	    }

	    $vcalendar = @file_get_contents($timezoneFilename);
	    if ($vcalendar !== false) {

	        $m = null;
	        preg_match('/(BEGIN:VTIMEZONE.*END:VTIMEZONE)/Us', $vcalendar, $m);

	        $vtimezone = $m[0];
	        $vtimezone = str_replace("\n", "\r\n", $vtimezone);
	        $vtimezone .= "\r\n";
	        return $vtimezone;
	    }

 		$vtimezone = 'BEGIN:VTIMEZONE' . "\r\n"
                    . 'TZID:Europe/Paris' . "\r\n"
                    . 'X-LIC-LOCATION:Europe/Paris' . "\r\n"
                    . 'BEGIN:DAYLIGHT' . "\r\n"
                    . 'TZOFFSETFROM:+0100' . "\r\n"
                    . 'TZOFFSETTO:+0200' . "\r\n"
                    . 'TZNAME:CEST' . "\r\n"
                    . 'DTSTART:19700329T020000' . "\r\n"
                    . 'RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=3' . "\r\n"
                    . 'END:DAYLIGHT' . "\r\n"
                    . 'BEGIN:STANDARD' . "\r\n"
                    . 'TZOFFSETFROM:+0200' . "\r\n"
                    . 'TZOFFSETTO:+0100' . "\r\n"
                    . 'TZNAME:CET' . "\r\n"
                    . 'DTSTART:19701025T030000' . "\r\n"
                    . 'RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10' . "\r\n"
                    . 'END:STANDARD' . "\r\n"
                    . 'END:VTIMEZONE' . "\r\n";
 		return $vtimezone;
	}







	/**
	 * Sets the base url to access the CalDAV server.
	 *
	 * @param string $url  The URL for the calendar server
	 *
	 * @return Func_CalendarBackend_Caldav
	 */
	public function setServerUrl($url)
	{
		$this->serverUrl = $url;

		return $this;
	}
	
	
	/**
	 * 
	 * @param int $userId
	 * @return int | null
	 */
	public function getUserServerId($userId)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibCaldav/Users/' . $userId);
		return $registry->getValue('server');
	}

	public function getUniqueId($userId)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibCaldav/Users/' . $userId);
		return $registry->getValue('uniqueId');
	}

	
	/**
	 * Get server configuration
	 * @param int $id_server
	 * @return array
	 */
	public function getServer($id_server)
	{
		global $babDB;
		
		$res = $babDB->db_query('SELECT * FROM libcaldav_servers WHERE id='.$babDB->quote($id_server));
		while($arr = $babDB->db_fetch_assoc($res))
		{
			return $arr;
		}
		
		return null;
	}

	public function getServerNeedId($userid)
	{
		global $babDB;
		$id_server = $this->getUserServerId($userid);
		$res = $babDB->db_query('SELECT * FROM libcaldav_servers WHERE id='.$babDB->quote($id_server));
		while($arr = $babDB->db_fetch_assoc($res))
		{
			if($arr["use_unique_id"]=="true")
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		return null;
	}



	/**
	 * returns the base url to access the CalDAV server.
	 * @param	int	$userId
	 * @return string
	 */
	public function getServerUrl($userId)
	{
		if (!isset($this->serverUrl)) {
			$id_server = $this->getUserServerId($userId);
			if (!isset($id_server))
			{
				return null;
			}
			
			$server = $this->getServer($id_server);
			
			return $server['server_url'];
		}
		return $this->serverUrl;
	}




	/**
	 * Returns the href identifier of a caldav user corresponding to an
	 * ovidentia user.
	 *
	 * @param int	$userId		An ovidentia user id.
	 */
	public function getUserHref($userId)
	{
		if ($this->configByUser()) {
		
			return $this->getUserIdentifier($userId);
		}
		
		return bab_getUserNickname($userId);
	}




	/**
	 * check a personnal calendar with user configuration
	 *
	 * @param int	$userId
	 *
	 * @return bool
	 */
	public function checkCalendar($userId)
	{
		$path = $this->getPersonnalCalendarUrl($userId);
		list($nickname, $password) = $this->getPersonalCalendarCredentials($userId);
		
		$this->caldavClient = new CalDAVClient($path, $nickname, $password, 'calendar');
		

		$xml = '<?xml version="1.0" encoding="utf-8" ?>
		<D:propfind xmlns:D="DAV:">
		<D:prop>
		<D:current-user-privilege-set/>
		</D:prop>
		</D:propfind>';


		try {
			$this->caldavClient->SetDepth(0);
			$response = $this->caldavClient->DoXMLRequest('PROPFIND', $xml, '');

		} catch (Exception $e) {
			return false;
		}

		return !empty($response);
	}

	/**
	 *
	 * @param caldav_EventCalendar $calendar
	 * @param $relativePath
	 *
	 * @return Func_CalendarBackend_Caldav
	 */
	public function init(caldav_EventCalendar $calendar)
	{	
		$this->caldavClient = $calendar->getCaldavClient();
		return $this;
	}



	/**
	 * The list of calendars recorded with the sharing access form
	 * to use theses calendars, the user must have a personal calendar or $babBody->babsite['iPersonalCalendarAccess'] == 'Y'
	 *
	 * @param int		$access_user		in most case, the current user
	 * @param string	$calendartype		optional filter by calendar type
	 * @return array	<int>				array of id_user
	 */
	public function getAccessiblePersonalCalendars($access_user = null, $calendartype = null)
	{
		/*@var $backend Func_CalendarBackend_Ovi */
		$backend = bab_functionality::get('CalendarBackend/Ovi');
		if (!isset($backend)) {
			return array();
		}

		return $backend->getAccessiblePersonalCalendars($access_user, $calendartype);
	}
	
	
	
	/**
	 * @return array
	 */
	public function getAccessibleResourceCalendars()
	{
		
		$arr = bab_getUserIdObjects('libcaldav_resource_groups');
		
		if (empty($arr))
		{
			return array();
		}
		
		global $babDB;
		
		$res = $babDB->db_query('SELECT * FROM libcaldav_resources WHERE id IN('.$babDB->quote($arr).')');
		$return = array();
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$return[$arr['id']] = array(
				'name' => $arr['name'],
				'url' => $arr['url'],
				'nickname' => $arr['nickname'],
				'password' => $arr['password']	
			);
		}
		
		return $return;
	}



	public function iCalObjectToVcalendarInvitation(bab_ICalendarObject $icalObject, bab_PersonalCalendar $calendar)
	{
		$this->includeCalendarPeriod();
		$vcalendar = 'BEGIN:VCALENDAR' . "\n"
		. $this->getProdId() . "\n"
		. 'VERSION:2.0' . "\n"
		. 'METHOD:REQUEST' . "\n"
		. $this->getTimeZone();

		$vcalendar .= caldav_CalendarPeriod::toIcal($icalObject) . "\n";

		$vcalendar .= 'END:VCALENDAR';

		return $vcalendar;
	}




	/**
	 * Creates or updates a calendar event.
	 *
	 * @param 	caldav_CalendarPeriod 	$period
	 * @param	string					$method		iCalendar Transport-Independent Interoperability Protocol (iTIP) (RFC 5546)
	 * 												PUBLISH | REQUEST | REPLY | ADD | CANCEL | REFRESH | COUNTER | DECLINECOUNTER
	 *
	 *
	 * @return bool		true if the period was correctly saved.
	 */
	public function savePeriod(bab_CalendarPeriod $period, $method = null)
	{
		$this->includeCalendarPeriod();
		$periodCollection = $period->getCollection();
		$calendar = $periodCollection->getCalendar();

		if (!($calendar instanceof caldav_EventCalendar))
		{
			throw new ErrorException(sprintf('failed to save the event %s, wrong calendar type %s', $period->getProperty('UID'), $calendar->getUrlIdentifier()));
		}

		$this->init($calendar);


		// Kerio specific :
		// when events selected from KERIO caldav server,
		// the ORGANIZER of an event is removed from the list of attendees
		// to fix this problem, we add X-CTO-ORGANIZER-PARTSTAT to store the PARTSTAT of the ORGANIZER
		// only if the organizer is in the list of attendees

		if ($organizer = $period->getOrganizer())
		{
			foreach($period->getAttendees() as $attendee)
			{
				if ($attendee['email'] === $organizer['email'] && $organizer['name'] === $attendee['CN'])
				{
					$period->setProperty('X-CTO-ORGANIZER-PARTSTAT', $attendee['PARTSTAT']);
					break;
				}
			}
		}


		$uid = $period->getProperty('UID');

		if (empty($uid)) {
			// create the event with generated ID
			$uid = uniqid();
			$period->setProperty('UID', $uid);
			$period->setProperty('SEQUENCE', 1);
			$period->setProperty('DTSTAMP', BAB_DateTime::now()->getICal(true));
			$period->setProperty('CREATED', BAB_DateTime::now()->getICal(true));
			$newEvent = true;
			$originalPeriods = array(clone $period);

		} else {
		    
		    
		    // optional delete on cancel
		    
		    $registry = bab_getRegistryInstance();
		    $registry->changeDirectory('/LibCaldav/');
		    
		    if ('CANCEL' === $method && $registry->getValue('deleteOnCancel')) {
		        return $this->deletePeriod($period);
		    }
		    

			// try to update SEQUENCE

			$sequence = 1 + (int) $period->getProperty('SEQUENCE');
			$period->setProperty('SEQUENCE', $sequence);

			$icalEvents = $this->caldavClient->GetEntryByUid($uid);
			$originalPeriods = null;

			foreach ($icalEvents as $icalEvent) {
			    
			    if (!isset($icalEvent['data'])) {
			        continue;
			    }
			    
				$originalPeriods = $this->expandPeriods($icalEvent['data'], $periodCollection, false);
			}


			if (isset($originalPeriods))
			{
				// Modification of an existing event.
				$newEvent = false;
			}
			else
			{
				// create the event with given ID
				$originalPeriods = array(clone $period);
				$newEvent = true;
			}

			/* @var $replacedPeriod bab_ICalendarObject */
			$replacedPeriod = null;

			$recurrenceId = $period->getProperty('RECURRENCE-ID');
			if ($recurrenceId !== '') {

				// Here we know we change only one recurrence period in the event.
				if (is_array($recurrenceId)) {
					$recurrenceId = array_pop($recurrenceId);
				}
				if (isset($originalPeriods[$recurrenceId])) {
					// The recurrence id already exist, we replace the period.
					$replacedPeriod = $originalPeriods[$recurrenceId];
				} else {
					// The recurrence id does not already exist, we create a new period recurrence.
					$replacedPeriod = null;
					$originalPeriods[$recurrenceId] = $period;
				}

			} else {

				// Here we know we change all the periods in the event.
				$replacedPeriod = reset($originalPeriods);
			}

			if (isset($replacedPeriod) && $replacedPeriod) {

				try {
					$replacedPeriod->removeAttendees();
					$replacedPeriod->removeRelations();
				} catch(Exception $e){
					// relation could not be removed if there is an ongoing approbation
				}

				foreach ($period->getProperties() as $icalProperty) {

					$colonPos = mb_strpos($icalProperty, ':');
					$propName = substr($icalProperty, 0, $colonPos);
					$propValue = substr($icalProperty, $colonPos + 1);
					

					if (!empty($propValue)) {
						$replacedPeriod->setProperty($propName, $propValue);
					}
				}

				foreach ($period->getRelations() as $relation) {
					$replacedPeriod->addRelation($relation['reltype'], $relation['calendar']);
				}

				foreach ($period->getAllAttendees() as $attendee) {
					if (isset($attendee['calendar']))
					{
						// the calendar of attendee is accessible
						$replacedPeriod->addAttendee($attendee['calendar'], $attendee['ROLE'], $attendee['PARTSTAT'], $attendee['RSVP']);
					} else {
						// the calendar is not accessible
						// try to get the non accessible calendar :
						
						if ($id_user = bab_getUserIdByEmailAndName($attendee['email'], $attendee['CN']))
						{
							// use the icalendars object of attendee to bypass the access rights tests
							$att_cals = bab_getICalendars($id_user);
							
							if( $id_calendar = $att_cals->getPersonalCalendarUid($id_user))
							{
								$reftype = $att_cals->getUserReferenceType($id_user);
								$usercal = $att_cals->getEventCalendar("$reftype/$id_calendar");
								
								if (isset($usercal))
								{
									$replacedPeriod->addAttendee($usercal, $attendee['ROLE'], $attendee['PARTSTAT'], $attendee['RSVP']);
								}
							}
						}
						
						
						
					}
				}

				if ($alarm = $period->getAlarm()) {
					$replacedPeriod->setAlarm($alarm);
				}
			}
		}
		
		
		if (empty($originalPeriods)) {
		    throw new ErrorException('No periods to save, put request canceled');
		}
		

		$xml = 'BEGIN:VCALENDAR' . "\n"
		. $this->getProdId() . "\n"
		. 'VERSION:2.0' . "\n"
		. $this->getTimeZone();


		if (null !== $method)
		{
			$xml .= "METHOD:$method\n";
		}


		foreach ($originalPeriods as $p) {
			$xml .= caldav_CalendarPeriod::toIcal($p) . "\n";
		}

		$xml .= 'END:VCALENDAR';


		if ($newEvent) {
			$this->caldavClient->setMatch(false);
		} else {
			// Check ETAG with If-Match (setMatch method)
		}
		
		try {
			$this->caldavClient->DoPUTRequest($uid . '.ics', $xml);
		} catch (Exception $e) {
		    
		    bab_debug($e->getFile().' -> '.$e->getLine().'<br /><br />'.$e->getTraceAsString());
		    
		    // only ErrorException are captured as error messages in ovidentia
            throw new ErrorException($e->getMessage());
		}

		// caldav_SessionFile($uid . '.ics', $xml);

		return true;
	}


	/**
	 * Process coma separated values from multiples properties or one property
	 * 
	 * T6775 : probablement plusieurs EXDATE dans le meme evenement
	 * 
	 * @param	string | array	$properties
	 * @return array
	 */
	private function explodeRules($properties)
	{
		if (is_array($properties))
		{
			$values = array();
			foreach($properties as $property)
			{
				$values = array_merge($values, explode(',', $property));
			}
			
			return $values;
		}
		
		return explode(',', $properties);
	}


	/**
	 * Expands an icalendar-formatted data string $icalEventData to an array of <bab_CalendarPeriod>.
	 * The data string must correspond to a single caldav event (only one UID) that can however be
	 * made of several recurrences (hence several VEVENT).
	 *
	 * @param string						$icalEventData		The icalendar-formatted event data.
	 * @param bab_CalendarEventCollection	$collection			If specified, periods will be associated to this collection.
	 * @param bool							$expandRecurrence	True to have VEVENTs with RRULES expanded.
	 * 															If set to false, only one bab_CalendarPariod is created for a
	 * 															VEVENT with an RRULE, VEVENTs with a RECURRENCE-ID are still
	 * 															returned as additional bab_CalendarPeriods.
	 *
	 * @return bab_CalendarPeriod[]
	 */
	protected function expandPeriods($icalEventData, bab_CalendarEventCollection $collection = null, $expandRecurrence = true, $expandStart = null, $expandEnd = null)
	{
		$periods = array();
		$eventsData = array();
		preg_match_all('/BEGIN:VEVENT(.*)END:VEVENT/Us', $icalEventData, $eventsData);

		if ($expandRecurrence) {
			// If recurrent events should be expanded and no expand range has been specified, we
			// limit to +/- 5 years.
			if (!isset($expandStart)) {
				require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
				$expandStart = BAB_DateTime::now();
				$expandStart->add(-5, BAB_DATETIME_YEAR);
				$expandStart = $expandStart->getICal();
			}
			if (!isset($expandEnd)) {
				require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
				$expandEnd = BAB_DateTime::now();
				$expandEnd->add(5, BAB_DATETIME_YEAR);
				$expandEnd = $expandEnd->getICal();
			}
		}

		foreach ($eventsData[1] as $eventData) {
			

			$period = $this->CalendarPeriod();

			try {
				$period->fromIcal($eventData);
			}
			catch(Exception $e)
			{
				// bab_debug($e->getMessage(), DBG_ERROR, 'caldav');
				continue;
			}

			if ($expandRecurrence
				&& ($period->getProperty('RRULE') !== '' || $period->getProperty('RDATE') !== '')) {

				// If the period has a recurrence rule, we create as much periods as necessary.

				require_once dirname(__FILE__) . '/caldav/RRule.php';

				$recurrenceDates = array();

				// First we use the RDATE and RRULE parameters to determine the list
				// of dates.

				$icalStart = new LibCaldav_iCalDate($period->getProperty('DTSTART'));
				$icalEnd = new LibCaldav_iCalDate($period->getProperty('DTEND'));
				$icalDuration = $icalStart->DateDifference($icalEnd);

				if ($period->getProperty('RDATE') !== '') {
					$rdates = $this->explodeRules($period->getProperty('RDATE'));
					foreach ($rdates as $rdate) {
						if ($rdate >= $expandStart && $rdate < $expandEnd) {
							$recurrenceDates[$rdate] = $rdate;
						}
					}
				}

				if ($period->getProperty('RRULE') !== '') {
					$rrule = new RRule($icalStart, $period->getProperty('RRULE'));

					// We add an element to $recurrenceDates for each date defined by the recurrence rule.
					while (($dtStart = $rrule->GetNext()) && ($dtStart->_text < $expandEnd)) {
						if ($dtStart->_text >= $expandStart) {
							$recurrenceDates[$dtStart->_text] = $dtStart->_text;
						}
					}
				}

				// Now we use EXDATE and EXRULE parameters to determine a list
				// of date excluded from the recurrence rule.

				if ($period->getProperty('EXDATE') !== '') {
					$exdates = $this->explodeRules($period->getProperty('EXDATE'));
					foreach ($exdates as $exdate) {
						if ($exdate >= $expandStart && $exdate < $expandEnd) {
							unset($recurrenceDates[$exdate]);
						}
					}
				}

				if ($period->getProperty('EXRULE') !== '') {
					$exrule = new RRule($icalStart, $period->getProperty('EXRULE'));

					// We remove an element from $recurrenceDates for each date defined by the recurrence rule.
					while (($dtStart = $exrule->GetNext()) && ($dtStart->_text < $expandEnd)) {
						if ($dtStart->_text >= $expandStart) {
							unset($recurrenceDates[$dtStart->_text]);
						}
					}
				}

				// Finally we create a period for each date remaining in $recurrenceDates.

				foreach ($recurrenceDates as $startDate) {
					$dtStart = new LibCaldav_iCalDate($startDate);
					$dtEnd = new LibCaldav_iCalDate($dtStart);
					$dtEnd->addDuration($icalDuration);
					
					// en utilisant un clone de $period c'est probablement plus rapide
					// mais il semble que le lien avec la collection soit casse, cela declanche un probleme dans bab_CalAttendeeBackend::getRealAttendee()
					// $date_period = clone $period;
					$date_period = $this->CalendarPeriod();
					$date_period->fromIcal($eventData);
					
					$date_period->setDates(BAB_DateTime::fromICal($dtStart->_text), BAB_DateTime::fromICal($dtEnd->_text));
					if (isset($collection)) {
						$collection->addPeriod($date_period);
						$relations = $date_period->getRelations('PARENT');
						if (count($relations) === 0) {
							$date_period->addRelation('PARENT', $collection->getCalendar());
						}
					}
					$periods[$date_period->getProperty('DTSTART')] = $date_period;
				}


			} else {

				if (isset($collection)) {
					$collection->addPeriod($period);
					$relations = $period->getRelations('PARENT');
					if (count($relations) === 0) {
						$period->addRelation('PARENT', $collection->getCalendar());
					}
				}
				if (!$expandRecurrence || ($period->getProperty('DTSTART') < $expandEnd && $period->getProperty('DTEND') > $expandStart) ) {
					$recurrenceId = $period->getProperty('RECURRENCE-ID');
					if ($recurrenceId !== '') {
						if (is_array($recurrenceId)) {
							$recurrenceId = array_pop($recurrenceId);
						}
						$periods[$recurrenceId] = $period;
					} else {
						$periods[$period->getProperty('DTSTART')] = $period;
					}
				}

			}
		}

		return $periods;
	}


	/**
	 * Returns the period corresponding to the specified identifier (and optionally a particular occurence at the specified start date).
	 *
	 * @param	bab_PeriodCollection	$periodCollection		where to search for event
	 * @param 	string 					$identifier				The UID property of event
	 * @param 	string 					$start					The icalendar-formatted start date (useful when editing a particular iteration of a recurrent event).
	 *
	 * @return caldav_CalendarPeriod	Or null if no match
	 */
	public function getPeriod(bab_PeriodCollection $periodCollection, $identifier, $start = null)
	{
		$calendar = $periodCollection->getCalendar();

		if (!$calendar)
		{
			throw new Exception(sprintf('try to search for event in collection %s but the collection is not in a calendar', get_class($periodCollection)));
		}

		$this->init($calendar);

		try {
			$events = $this->caldavClient->GetEntryByUid($identifier);
		} catch (Exception $e) {
			bab_debug($e->getMessage());
			$events = array();
		}

		$periods = array();

		//		var_dump($events);
		foreach ($events as $icalEvent) {
		    
		    if (!isset($icalEvent['data'])) {
		        continue;
		    }

//			echo $icalEvent['data'] . "\n\n\n";
			$periods = $this->expandPeriods($icalEvent['data'], $periodCollection, true);
			if (isset($start) && isset($periods[$start])) {
				return $periods[$start];
			}


			if ($identifier.'.ics' === $icalEvent['href']) {
				return array_pop($periods);
			}
		}

		if (count($periods) > 0)
		{
			return array_pop($periods);
		}


		return null;
	}


	/**
	 * Returns the period corresponding to the specified identifier (and optionally a particular occurence at the specified start date).
	 *
	 * @param bab_PeriodCollection	$periodCollection		where to search for event
	 * @param string 				$identifier				The UID property of event
	 * @param bool					$expandRecurrence		True to have VEVENTs with RRULES expanded.
	 * 														If set to false, only one bab_CalendarPariod is created for a
	 * 														VEVENT with an RRULE, VEVENTs with a RECURRENCE-ID are still
	 * 														returned as additional bab_CalendarPeriods.
	 * @param BAB_DateTime			$expandStart			Lower limit for date expansion, null for no lower limit.
	 * @param BAB_DateTime			$expandEnd				Upper limit for date expansion, null for no upper limit.
	 *
	 * @return array<caldav_CalendarPeriod>	Or null if no match
	 */
	public function getAllPeriods(bab_PeriodCollection $periodCollection, $identifier, $expandRecurrence = true, BAB_DateTime $expandStart = null, BAB_DateTime $expandEnd = null)
	{
		$calendar = $periodCollection->getCalendar();

		$this->init($calendar);
		$events = $this->caldavClient->GetEntryByUid($identifier);

		if (isset($expandStart)) {
			$expandStart = $expandStart->getICal();
		}
		if (isset($expandEnd)) {
			$expandEnd = $expandEnd->getICal();
		}
		$periods = array();

		foreach ($events as $icalEvent) {

			$periods = $this->expandPeriods($icalEvent['data'], $periodCollection, $expandRecurrence, $expandStart, $expandEnd);
			return $periods;
		}
		return null;
	}



	private static function flattenCriteria(bab_PeriodCriteria $criteria, &$criteriaArray)
	{
		$criteriaArray[] = $criteria;
		$subCriteria = $criteria->getCriterions();
		foreach($subCriteria as $subCriterion) {
			self::flattenCriteria($subCriterion, $criteriaArray);
		}
	}




	/**
	 * @param bab_PeriodCriteria $criteria
	 *
	 * @return iterator <caldav_CalendarPeriod>
	 */
	public function selectPeriods(bab_PeriodCriteria $criteria = null)
	{
		$criteriaArray = array();
		self::flattenCriteria($criteria, $criteriaArray);

		$filteredProperties = array();

		$begin = null;
		$end = null;
		$uid_values = array();
		$uid_criterion = null;

		foreach ($criteriaArray as $criterion) {
			//			echo get_class($criterion) . "\n";
			if ($criterion instanceof bab_PeriodCriteriaCalendar) {
				$selectedCalendars = array();
				foreach ($criterion->getCalendar() as $calendarId => $calendar) {
					if (($calendar instanceof caldav_PersonalCalendar) || ($calendar instanceof caldav_ResourceCalendar)) {
						$selectedCalendars[$calendarId] = $calendar;
					}
				}
			} else if ($criterion instanceof bab_PeriodCritieraBeginDateLessThanOrEqual) {
				$end = $criterion->getDate();
				$end = isset($end) ? $end->getICal(true) : null;
			} else if ($criterion instanceof bab_PeriodCritieraEndDateGreaterThanOrEqual) {
				$begin = $criterion->getDate();
				$begin = isset($begin) ? $begin->getICal(true) : null;

			} else if ($criterion instanceof bab_PeriodCritieraUid) {

				$uid_criterion = clone $criterion;
				$uid_values = $criterion->getUidValues();

				foreach($uid_values as $uid)
				{
					$filteredProperties[] = array(
						'name' => 'UID',
						'value' => $uid,
						'contain' => false
					);
				}

			} else if ($criterion instanceof bab_PeriodCritieraProperty) {

				foreach ($criterion->getValue() as $value) {
					$filteredProperties[] = array(
						'name' => $criterion->getProperty(),
						'value' => $value,
						'contain' => $criterion->getContain()
					);
				}
			}
		}


		if (empty($selectedCalendars))
		{
			require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
			bab_debug('Warning : query in LibCaldav without compatible calendars', DBG_WARNING, 'Caldav');
			return array();
		}


		$periods = array();

		foreach ($selectedCalendars as $selectedCalendar) {

			try {
				$this->init($selectedCalendar);
				
				if (null === $begin && null === $end && $uid_values)
				{
					// getUidValuesByCalendar use the parent_calendar column in the inbox
					$uid_list = $uid_criterion->getUidValuesByCalendar($selectedCalendar);
					if (!$uid_list)
					{
						continue;
					}
					
					$events = $this->caldavClient->GetEntriesByUid($uid_list);
				} else {
					$this->caldavClient->SetDepth(1);
					$events = $this->caldavClient->GetEvents($begin, $end);
				}
				
			} catch (caldav_HttpException $e) // 403, 404
			{
				global $babBody;
				$babBody->addError($e->getMessage()."test");
				return array();
				
			}
			 catch (caldav_AccessException $e) // 401
			{
				global $babBody;
				$babBody->addError($e->getMessage()."test2");
				return array();
				
			} catch (Exception $e) {
				bab_debug($e->getMessage());
				continue; 	// exemple: the inbox of a ovidentia user contain a reference to a caldav event in a broken
							// user configuration, the continue allow to process other caldav calendars in the same inbox
			}


			$defaultCollection = $this->CalendarEventCollection($selectedCalendar);
			//		$defaultCollection->setCalendar($calendar[0]);

			foreach ($events as $event) {

				if (!isset($event['data']))
				{
					continue;
				}


				$ps = $this->expandPeriods($event['data'], $defaultCollection, true, $begin, $end);
				foreach ($ps as $p) {
					$periods[] = $p;
					$this->caldavClient->setHrefForUid($p->getProperty('UID'), $event['hrefpath']);
				}

				
			}

		}


		if (!empty($filteredProperties)) {

			$filteredPeriods = array();
			foreach ($periods as $period) {
				foreach ($filteredProperties as $property) {
					if (false === $property['contain'] && (((string) $property['value']) === (string) $period->getProperty($property['name'])))
					{
						$filteredPeriods[] = $period;
						break;

					} else if (stripos($period->getProperty($property['name']), $property['value']) !== false) {
						$filteredPeriods[] = $period;
						break;
					}
				}
			}


			$periods = $filteredPeriods;
		}


		/*
		foreach ($periods as $period) {
			bab_debug($period->getProperties(), 2, 'caldav');
		}


		require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
		bab_debug_print_backtrace();
		*/

		return $periods;
	}




	/**
	 * Deletes the period corresponding to the specified identifier.
	 *
	 * @param	bab_CalendarPeriod	$period		The period to delete
	 *
	 */
	public function deletePeriod(bab_CalendarPeriod $period)
	{

		$periodCollection = $period->getCollection();
		$calendar = $periodCollection->getCalendar();

		if ($calendar instanceof caldav_PersonalCalendar)
		{
			$this->deletePeriodForUser($calendar, $period);
		}
		else
		{
			// the main calendar is not caldav
			// try to get a caldav calendar from the attendees or throw an error
			foreach($period->getAttendees() as $attendee)
			{
				if ($attendee['calendar'] instanceof caldav_PersonalCalendar)
				{
					
					$this->deletePeriodForUser($attendee['calendar'], $period);
				}
			}
		}
	}







	/**
	 *
	 * @param caldav_PersonalCalendar $calendar
	 * @param bab_CalendarPeriod $period
	 * @return unknown_type
	 */
	private function deletePeriodForUser(caldav_PersonalCalendar $calendar, bab_CalendarPeriod $period)
	{
		$uid = $period->getProperty('UID');
		$periodCollection = $period->getCollection();
		$this->init($calendar);

		$recurrenceId = $period->getProperty('RECURRENCE-ID');

		if ($recurrenceId === '') {

			// No RECURRENCE-ID specified so we delete the whole event.
			try {
				$this->caldavClient->DoDELETERequest($uid . '.ics');
			} catch (Exception $e) {
				bab_debug($e->getMessage());
			}

			return true;

		}




		require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';

		// Here we know we delete recurrence period(s) in the event.

		if (is_array($recurrenceId)) {
			list($params , $recurrenceId) = each($recurrenceId);
			$params = explode(';',$params);
			array_shift($params);
			$range = null;

			foreach($params as $param)
			{
				$arr = explode('=', $param);
				if (2 === count($arr))
				{
					list($name, $value) = $arr;
					$name = trim($name);
					$value = trim($value);

					switch($name)
					{
						case 'RANGE':
							$range = $value;
							break;
					}
				}
			}
		}

		$originalPeriods = array();
		$icalEvents = $this->caldavClient->GetEntryByUid($uid);

		foreach ($icalEvents as $icalEvent) {
			$originalPeriods = $this->expandPeriods($icalEvent['data'], $periodCollection, false);
		}

		if ((!isset($originalPeriods[0])) || ($originalPeriods[0]->getProperty('RRULE') === '')) {
			$this->init($calendar);

			// no recurence rule, delete full event

			try {
				$this->caldavClient->DoDELETERequest($uid . '.ics');
			} catch (Exception $e) {

			}
			return true;
		}


		foreach($originalPeriods as $dtstart => $period)
		{
			if (0 !== $dtstart && ($dtstart === $recurrenceId || ($dtstart < $recurrenceId && 'THISANDPRIOR' === $range) || ($dtstart > $recurrenceId && 'THISANDFUTURE' === $range)) )
			{
				// A period with this recurrence id already exist in the list of modified periods, we remove the period.
				unset($originalPeriods[$dtstart]);
			}
		}


		// search the list of periods to add as exception

		$rrulePeriods = $this->expandPeriods($icalEvent['data'], $periodCollection, true);
		$deleteRuleExceptions = array();
		foreach($rrulePeriods as $dtstart => $period)
		{
			if ($dtstart === $recurrenceId || ($dtstart < $recurrenceId && 'THISANDPRIOR' === $range) )
			{
				$deleteRuleExceptions[$dtstart] = $dtstart;
			}
		}


		if (isset($originalPeriods[0]))
		{
			$originalPeriod = $originalPeriods[0];


			if ($deleteRuleExceptions) {
				// add exceptions dates

				if (($exdate = $originalPeriod->getProperty('EXDATE')) === '') {
					$exdate = implode(',', $deleteRuleExceptions);
				} else {
					$exdate = explode(',', $exdate);
					$exdate = array_merge($exdate, array_values($deleteRuleExceptions));
					$exdate = implode(',', $exdate);
				}
				$originalPeriod->setProperty('EXDATE', $exdate);
			}


			if ('THISANDFUTURE' === $range) {
				// update the RRULE with an UNTIL value (UTC is mandatory)

				if ($rrule = $originalPeriod->getProperty('RRULE'))
				{
					$until = BAB_DateTime::fromICal($recurrenceId);
					$until = $until->getICal(true);
					$rrule = preg_replace('/UNTIL=\s*[0-9TZ]+/', '', $rrule);
					$rrule = trim($rrule, ' ;');
					$rrule .= ';UNTIL='.$until;

					bab_debug($rrule);
					$originalPeriod->setProperty('RRULE', $rrule);
				}
			}

			$originalPeriod->setProperty('LAST-MODIFIED', BAB_DateTime::now()->getIcal(true));
			$originalPeriod->setProperty('DTSTAMP', BAB_DateTime::now()->getIcal(true));
		}


		if (count($originalPeriods) == 0) {
			return;
		}

		$xml = 'BEGIN:VCALENDAR' . "\n"
		. $this->getProdId() . "\n"
		. 'VERSION:2.0' . "\n"
		. $this->getTimeZone();

		foreach ($originalPeriods as $p) {
			$xml .= caldav_CalendarPeriod::toIcal($p) . "\n";
		}

		$xml .= 'END:VCALENDAR';

		$this->init($calendar);


		// TODO : Check ETAG with If-Match (setMatch method)

		try {
			$this->caldavClient->DoPUTRequest($uid . '.ics', $xml);
		} catch (Exception $e) {
		}

		return true;
	}



	/**
	 * Update an attendee PARTSTAT value of a calendar event
	 * a user can modifiy his participation status without modifing the full event, before triggering this method, the access right will be checked with the
	 * canUpdateAttendeePARTSTAT method of the calendar
	 *
	 * @see bab_EventCalendar::canUpdateAttendeePARTSTAT()
	 *
	 * @param bab_CalendarPeriod 	$period		the event
	 * @param bab_PersonalCalendar 	$calendar	the personal calendar used as an attendee
	 * @param string 				$partstat	ACCEPTED | DECLINED
	 * @param string				$comment	comment given when changing PARTSTAT (optional)
	 * @return bool
	 */
	public function updateAttendeePartstat(bab_CalendarPeriod $period, bab_PersonalCalendar $calendar, $partstat, $comment = '')
	{
		$period->addAttendee($calendar, 'REQ-PARTICIPANT', $partstat);

		$return = $this->savePeriod($period, 'REPLY');
		$period->commitEvent();

		return $return;
	}




	/**
	 * @param	int	$accessType		BAB_CAL_ACCESS_VIEW | BAB_CAL_ACCESS_UPDATE | BAB_CAL_ACCESS_FULL | BAB_CAL_ACCESS_SHARED_UPDATE
	 *
	 * return string		The dav xml privilege corresponding to the ovidentia access type.
	 */
	public function getDavPrivilege($accessType)
	{
		switch ($accessType) {

			// TODO : NEED to update right here.
			case BAB_CAL_ACCESS_VIEW:
				return '<D:privilege><D:read/></D:privilege>';

			case BAB_CAL_ACCESS_UPDATE:
				return '<D:privilege><D:read/></D:privilege><D:privilege><D:write/></D:privilege>';

			case BAB_CAL_ACCESS_FULL:
				return '<D:privilege><D:read/></D:privilege><D:privilege><D:write/></D:privilege>';

			case BAB_CAL_ACCESS_SHARED_UPDATE:
				return '<D:privilege><D:read/></D:privilege><D:privilege><D:write/></D:privilege>';
		}
		return '';
	}




	/**
	 * @param $object
	 * @param array			$privileges
	 * @param array<int>	$users			Ovidentia user ids
	 * @param string		$changeType		'grant' or 'deny'
	 */
	protected function changeAccess($object, $accessTypes, $users, $changeType)
	{
		if (!is_array($users)) {
			$users = array($users);
		}
		$xmlPrincipals = '<D:principal>';
		foreach ($users as $user) {
			$xmlPrincipals .= '<D:href>' . $this->getUserHref($user) . '</D:href>';
		}
		$xmlPrincipals .= '</D:principal>';

		if (!is_array($accessTypes)) {
			$accessTypes = array($accessTypes);
		}
		$xmlPrivileges = '<D:' . $changeType . '>';
		foreach ($accessTypes as $accessType) {
			$xmlPrivileges .= $this->getDavPrivilege($accessType);
		}
		$xmlPrivileges .= '</D:' . $changeType . '>';

		$xml =
			'<?xml version="1.0" encoding="utf-8" ?>
   			<D:acl xmlns:D="DAV:">
			 <D:ace>' .
		$xmlPrincipals .
		$xmlPrivileges .
	  	   '</D:ace>
	 	  </D:acl>';

		if (!is_string($object)) {
			$objectUrl = $object->getUrl();
		} else {
			$objectUrl = $object;
		}

		$userId = $GLOBALS['BAB_SESS_USERID'];
		$caldavClient = new CalDAVClient($this->getServerUrl(), $this->getUserIdentifier($userId),$this->getUserPassword($userId), null);
		try {
			return $caldavClient->DoXMLRequest('ACL', $xml, $objectUrl);
		} catch (Exception $e) {
		}
	}




	/**
	 */
	public function grantAccess($object, $accessTypes, $users)
	{
		$this->changeAccess($object, $accessTypes, $users, 'grant');
	}




	/**
	 */
	public function revokeAccess($object, $accessTypes, $users)
	{
		$this->changeAccess($object, $accessTypes, $users, 'deny');
	}



	/**
	 * Returns all access privileges granted to the specified user on the specified object.
	 *
	 * @return array
	 */
	public function getAccesses($object, $userId)
	{
		return;
		
		
		$xml = '<?xml version="1.0" encoding="utf-8" ?>
				<D:propfind xmlns:D="DAV:">
				<D:prop>
				<D:current-user-privilege-set/>
				</D:prop>
				</D:propfind>';
		if (!is_string($object)) {
			$objectUrl = $object->getUrl();
		} else {
			$objectUrl = $object;
		}
		try {
			$this->caldavClient->SetDepth(0);
			$this->caldavClient->DoXMLRequest('PROPFIND', $xml, $objectUrl);
		} catch(Exception $e) {

		}


		$dom = new DOMDocument();
		$dom->loadXML($this->caldavClient->xmlResponse);


		//		HTTP/1.1 207 Multi-Status
		//		Date: Thu, 09 Sep 2010 09:58:33 GMT
		//		Server: Apache/2.2.9 (Debian) PHP/5.2.6-1+lenny8 with Suhosin-Patch
		//		X-Powered-By: PHP/5.2.6-1+lenny8
		//		DAV: 1, 2, access-control, calendar-access, calendar-schedule, extended-mkcol, calendar-proxy, bind, calendar-auto-schedule
		//		ETag: "b4449ea2bf72f5e850ec928688e197b7"
		//		X-DAViCal-Version: DAViCal/0.9.9; DB/1.2.8
		//		Content-Length: 1541
		//		Vary: Accept-Encoding
		//		Connection: close
		//		Content-Type: text/xml; charset="utf-8"
		//
		//		<?xml version="1.0" encoding="utf-8" ? >
		//		<multistatus xmlns="DAV:" xmlns:C="urn:ietf:params:xml:ns:caldav">
		//		 <response>
		//		  <href>/davical/caldav.php/laucho/home/</href>
		//		  <propstat>
		//		   <prop>
		//		    <current-user-privilege-set>
		//		     <privilege>
		//		      <all/>
		//		     </privilege>
		//		     <privilege>
		//		      <read/>
		//		     </privilege>
		//		     <privilege>
		//		      <unlock/>
		//		     </privilege>
		//		     <privilege>
		//		      <read-acl/>
		//		     </privilege>
		//		     <privilege>
		//		      <read-current-user-privilege-set/>
		//		     </privilege>
		//		     <privilege>
		//		      <write-acl/>
		//		     </privilege>
		//		     <privilege>
		//		      <C:read-free-busy/>
		//		     </privilege>
		//		     <privilege>
		//		      <write/>
		//		     </privilege>
		//		     <privilege>
		//		      <write-properties/>
		//		     </privilege>
		//		     <privilege>
		//		      <write-content/>
		//		     </privilege>
		//		     <privilege>
		//		      <bind/>
		//		     </privilege>
		//		     <privilege>
		//		      <unbind/>
		//		     </privilege>
		//		     <privilege>
		//		      <C:schedule-deliver/>
		//		     </privilege>
		//		     <privilege>
		//		      <C:schedule-deliver-invite/>
		//		     </privilege>
		//		     <privilege>
		//		      <C:schedule-deliver-reply/>
		//		     </privilege>
		//		     <privilege>
		//		      <C:schedule-query-freebusy/>
		//		     </privilege>
		//		     <privilege>
		//		      <C:schedule-send/>
		//		     </privilege>
		//		     <privilege>
		//		      <C:schedule-send-invite/>
		//		     </privilege>
		//		     <privilege>
		//		      <C:schedule-send-reply/>
		//		     </privilege>
		//		     <privilege>
		//		      <C:schedule-send-freebusy/>
		//		     </privilege>
		//		    </current-user-privilege-set>
		//		   </prop>
		//		   <status>HTTP/1.1 200 OK</status>
		//		  </propstat>
		//		 </response>
		//		</multistatus>

		$xpath = new DOMXPath($dom);

		$xpath->registerNamespace('D', 'DAV:');

		$entries = $xpath->query('//D:multistatus/D:response/D:propstat/D:prop/D:current-user-privilege-set/D:privilege/*');


		$accesses = array();
		//BAB_CAL_ACCESS_VIEW | BAB_CAL_ACCESS_UPDATE | BAB_CAL_ACCESS_FULL | BAB_CAL_ACCESS_SHARED_UPDATE
		foreach ($entries as $entry) {

    		switch ($entry->tagName) {
    			case 'all':
    				$accesses[BAB_CAL_ACCESS_FULL] = BAB_CAL_ACCESS_FULL;
    				break;
    			case 'write':
    				$accesses[BAB_CAL_ACCESS_UPDATE] = BAB_CAL_ACCESS_UPDATE;
    				break;
    			case 'read':
    				$accesses[BAB_CAL_ACCESS_VIEW] = BAB_CAL_ACCESS_VIEW;
    				break;
    		}
		}

		return $accesses;
	}

	/**
	 * The default caldav server url if no other is explicitely specified.
	 * @deprecated
	 * @return string
	 */
	public function getDefaultServerUrl()
	{
		return '';
	}




	/**
	 *
	 * @param int	$userId
	 * @param int	$serverId
	 *
	 * @return Func_CalendarBackend_Caldav
	 */
	public function setServerId($userId, $serverId)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibCaldav/Users/' . $userId);
		$registry->setKeyValue('server', $serverId);
	}
	
	
	
	




	/**
	 * The default url for any user's personal calendar
	 * %u will be replaced by ovidentia nickname
	 *
	 * @return string
	 */
	public function getDefaultUserCalendarUrl()
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibCaldav');
		$path = $registry->getValue('defaultUserCalendarUrl', '');
		return $path;
	}




	/**
	 *
	 *
	 * @param string	$path
	 *
	 * @return Func_CalendarBackend_Caldav
	 */
	public function setDefaultUserCalendarUrl($path)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibCaldav');
		$registry->setKeyValue('defaultUserCalendarUrl', $path);
	}




	/**
	 * The caldav admin identifier
	 *
	 * @return string
	 */
	public function getAdminIdentifier()
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibCaldav');
		$identifier = $registry->getValue('adminIdentifier', '');
		return $identifier;
	}




	/**
	 *
	 *
	 * @param string	$identifier
	 *
	 * @return Func_CalendarBackend_Caldav
	 */
	public function setAdminIdentifier($identifier)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibCaldav');
		$registry->setKeyValue('adminIdentifier', $identifier);
	}




	/**
	 * The caldav admin password
	 *
	 * @return string
	 */
	public function getAdminPassword()
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibCaldav');
		$password = $registry->getValue('adminPassword', '');
		return $password;
	}




	/**
	 *
	 *
	 * @param string	$password
	 *
	 * @return Func_CalendarBackend_Caldav
	 */
	public function setAdminPassword($password)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibCaldav');
		$registry->setKeyValue('adminPassword', $password);
	}
	
	
	
	/**
	 * Set if backend is used as a storage backend (allow user switching)
	 *
	 * @param bool		$status
	 *
	 * @return Func_CalendarBackend_Caldav
	 */
	public function setStorageBackend($status)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibCaldav');
		$registry->setKeyValue('StorageBackend', (bool) $status);
	}

	
	
	public function getStorageBackend()
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibCaldav');
		return $registry->getValue('StorageBackend', true);
	}
	
	
	
	public function setVerifyPeer($status)
	{
	    $registry = bab_getRegistryInstance();
	    $registry->changeDirectory('/LibCaldav');
	    $registry->setKeyValue('verify_peer', (bool) $status);
	}
	
	
	
	public function getVerifyPeer()
	{
	    $registry = bab_getRegistryInstance();
	    $registry->changeDirectory('/LibCaldav');
	    return $registry->getValue('verify_peer', true);
	}
	
	/**
	 * test if personnal calendar use a per user configuration
	 * @return bool
	 */
	public function configByUser()
	{
		return ('' === $this->getDefaultUserCalendarUrl());
	}
	
	
	/**
	 * Get username and password to use for personnal calendar
	 * @return array
	 */
	public function getPersonalCalendarCredentials($userId)
	{
		if ($this->configByUser())
		{
			$nickname = $this->getUserIdentifier($userId);
			$password = $this->getUserPassword($userId);
		} else {
			$nickname = $this->getAdminIdentifier();
			$password = $this->getAdminPassword();
		}
		
		return array($nickname, $password);
	}
	
	
	
	public function getPersonnalCalendarUrl($userId)
	{
		if ($this->configByUser())
		{

				$relativePath = $this->getUserCalendarPath($userId);
				
				$url = $this->getServerUrl($userId) . $relativePath;
				if (empty($url))
				{
					throw new Exception(sprintf('Empty calendar url for user %s', bab_getUserName($userId)));
				}
				
				if (substr($url, -1, 1) !== '/') {
					$url .= '/';
				}
				return $url;
			
		} else {
			
			$calendarPathTemplate = $this->getDefaultUserCalendarUrl();
			return $this->processCalendarPathTemplate($calendarPathTemplate, bab_getUserNickname($userId), $userId);
		}
	}



	/**
	 * Returns the stored caldav identifier for ovidentia user $userId.
	 *
	 * @param int $userId The ovidentia user id.
	 *
	 * @return string
	 */
	public function getUserIdentifier($userId)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibCaldav/Users/' . $userId);
		$identifier = $registry->getValue('identifier');
		if (!isset($identifier)) {
			return bab_getUserNickname($userId);
		}
		return $identifier;
	}




	/**
	 * Sets the caldav identifier for ovidentia user $userId.
	 *
	 * @param string	$identifier
	 *
	 * @param int    $userId     The ovidentia user id.
	 * @param string $identifier The caldav user identifier.
	 *
	 * @return Func_CalendarBackend_Caldav
	 */
	public function setUserIdentifier($userId, $identifier)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibCaldav/Users/' . $userId);
		$registry->setKeyValue('identifier', $identifier);
	}




	/**
	 * Returns the stored caldav password for ovidentia user $userId.
	 *
	 * @param int $userId The ovidentia user id.
	 *
	 * @return string
	 */
	public function getUserPassword($userId)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibCaldav/Users/' . $userId);
		$password = $registry->getValue('password', '');
		return $password;
	}




	/**
	 * Sets the caldav password for ovidentia user $userId.
	 *
	 * @param string	$password
	 *
	 * @param int    $userId     The ovidentia user id.
	 * @param string $identifier The caldav user identifier.
	 *
	 * @return Func_CalendarBackend_Caldav
	 */
	public function setUserPassword($userId, $password)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibCaldav/Users/' . $userId);
		$registry->setKeyValue('password', $password);
	}

	public function setUniqueId($UserId,$uniqueId)
	{
	    $registry = bab_getRegistryInstance();
	    $registry->changeDirectory('/LibCaldav/Users/' . $UserId);
	    $registry->setKeyValue('uniqueId', $uniqueId);
	    
	}

	/**
	 * Returns the relative caldav path to the specified user's personal calendar.
	 *
	 * @param int $userId The ovidentia user id.
	 *
	 * @return string
	 */
	public function getUserCalendarPath($userId)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibCaldav/Users/' . $userId);
		$path = $registry->getValue('calendarPath');

		if (!isset($path)) {
			
			$id_server = $this->getUserServerId($userId);
			if (!isset($id_server))
			{
				return null;
			}
			$server = $this->getServer($id_server);
			$nickname = $this->getUserIdentifier($userId);
			$path = $this->processCalendarPathTemplate($server['user_calendar_path'], $nickname, $userId);
			
			
		}
		return $path;
	}
	
	/**
	 * Process replacements in calendar path
	 * 
	 * @param	string	$calendarPathTemplate	ex : /%u/home
	 * @param	string	$nickname				Nickname as typed by the user
	 * 
	 * @return string
	 */
	public function processCalendarPathTemplate($calendarPathTemplate, $nickname, $userid)
	{
		
		if (false !== $p = mb_strpos($nickname, '@'))
		{
			$n = mb_substr($nickname, 0, $p);
		} else {
			$n = $nickname;
		}
		if($this->getServerNeedId($userid))
		{
			$uniqueId = $this->getUniqueId($userid);
			
		}
		else
		{
			$uniqueId = " ";
		}
		
		return str_replace(array('%u', '%n', '%c'), array($nickname, $n,$uniqueId), $calendarPathTemplate);
	}




	/**
	 * Get url of an option page for the backend, the page will be displayed in a popup window accessible for each user
	 * from the calendar options.
	 *
	 * @return string
	 */
	public function getOptionsUrl()
	{
		$addon = bab_getAddonInfosInstance('LibCaldav');

		if (!$addon || !$this->configByUser())
		{
			return '';
		}

		return $addon->getUrl().'userconfiguration';
	}
}
