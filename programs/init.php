<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2010 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/functions.php';

function LibCaldav_upgrade($version_base, $version_ini)
{
	require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	include_once $GLOBALS['babInstallPath']."utilit/eventincl.php";
	
	if (!@bab_functionality::includefile('CalendarBackend')) {
		return false;
	}

	$addonName = 'LibCaldav';
	$addonInfo = bab_getAddonInfosInstance($addonName);
	
	$addonPhpPath = $addonInfo->getPhpPath();
	
	$functionalities = new bab_functionalities();
	$functionalities->registerClass('Func_CalendarBackend_Caldav', $addonPhpPath . 'caldav.class.php');

	$addonInfo->removeAllEventListeners();
	$addonInfo->addEventListener('bab_eventBeforePeriodsCreated', 'caldav_onBeforePeriodsCreated', 'eventperiod.php');
	$addonInfo->addEventListener('bab_eventBeforePeriodsCreated', 'caldav_onBeforePeriodsCreated', 'eventperiod.php');
	$addonInfo->addEventListener('bab_eventCollectCalendarsBeforeDisplay', 'caldav_onCollectCalendarsBeforeDisplay', 'eventperiod.php');
	$addonInfo->addEventListener('bab_eventPageRefreshed', 'LibCaldav_onPageRefreshed', 'init.php');
	$addonInfo->addEventListener('bab_eventUserCreated', 'LibCaldav_onUserCreated', 'init.php');
	
	$tables = new bab_synchronizeSql();
	$tables->fromSqlFile(dirname(__FILE__).'/tables.sql');
	
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/LibCaldav/');
	
	if ($serverUrl = $registry->getValue('defaultServerUrl'))
	{
		// move default server from registry to server table
		
		$userCalendarPath = $registry->getValue('defaultUserCalendarPath');
		
		$url = parse_url($serverUrl);
		if (isset($url['host']))
		{
			$host = $url['host'];
		} else {
			$host = 'Serveur';
		}
		$useuniqueid = 'false';
		global $babDB;
		$babDB->db_query('INSERT INTO libcaldav_servers (name, server_url, user_calendar_path, use_unique_id) 
				VALUES ('.$babDB->quote($host).', '.$babDB->quote($serverUrl).', '.$babDB->quote($userCalendarPath).', '.$babDB->quote($useuniqueid).')');
		
		$id_server = (int) $babDB->db_insert_id();
		
		$registry->removeKey('defaultUserCalendarPath');
		$registry->removeKey('defaultServerUrl');
		
		// browse users
		
		$registry->changeDirectory('/LibCaldav/Users/');
		$keys = array();
		while ($id_user = $registry->fetchChildDir())
		{
			$keys[] = $id_user;
		}
		
		foreach($keys as $dirkey)
		{
			$registry->changeDirectory("/LibCaldav/Users/$dirkey");
			$registry->setKeyValue('server', $id_server);
		}
	}
	
	return true;
}

$GLOBALS['LibCaldav_AdminSectionMenus'] = array(
	'CalDAV configuration' => $GLOBALS['babAddonUrl'].'configuration'
);

function LibCaldav_getAdminSectionMenus(&$url, &$text)
{
	global $LibCaldav_AdminSectionMenus;
	if (list($text, $url) = each($LibCaldav_AdminSectionMenus))
	{
		$text =  caldav_translate($text);
		return true;
	}
	reset($LibCaldav_AdminSectionMenus);
	return false;
}




function LibCaldav_onDeleteAddon()
{
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	include_once $GLOBALS['babInstallPath']."utilit/eventincl.php";
	
	
	bab_removeEventListener('bab_eventBeforePeriodsCreated'			,'caldav_onBeforePeriodsCreated'			,'addons/LibCaldav/eventperiod.php');
	bab_removeEventListener('bab_eventCollectCalendarsBeforeDisplay','caldav_onCollectCalendarsBeforeDisplay'	,'addons/LibCaldav/eventperiod.php');
	bab_removeEventListener('bab_eventPageRefreshed', 'LibCaldav_onPageRefreshed', 'addons/LibCaldav/init.php');
	bab_removeEventListener('bab_eventUserCreated', 'LibCaldav_onUserCreated', 'addons/LibCaldav/init.php');
	
	$functionalities = new bab_functionalities();
	$functionalities->unregister('CalendarBackend/Caldav');
}




function LibCaldav_onPageRefreshed(bab_eventPageRefreshed $event)
{
	// afficher les stats de connexion a caldav
	
	/*
	if (!isset($_SESSION['LibCaldav']['stats']))
	{
		return;
	}
	
	if (isset($_SESSION['LibCaldav']['stats']['pending'])) {
		$_SESSION['LibCaldav']['stats'][date('H:i:s')] = array_values($_SESSION['LibCaldav']['stats']['pending']);
		unset($_SESSION['LibCaldav']['stats']['pending']);
	}
	
	// get only the last 10 queries
	if (count($_SESSION['LibCaldav']['stats']) > 10)
	{
		$_SESSION['LibCaldav']['stats'] = array_slice($_SESSION['LibCaldav']['stats'], -10, 10, true);
	}
	
	
	bab_debug($_SESSION['LibCaldav']['stats'], DBG_TRACE, 'Caldav statistics');
	*/
}



function LibCaldav_onUserCreated(bab_eventUserCreated $event)
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/LibCaldav/');
	
	if ($registry->getValue('default_backend'))
	{
		require_once $GLOBALS['babInstallPath'].'utilit/calapi.php';
		
		/* @var $caldav Func_CalendarBackend_Caldav */
		$caldav = bab_functionality::get('CalendarBackend/Caldav');
		
		bab_setPersonnalCalendarBackend($event->id_user, $caldav);
	}
	
	
}
