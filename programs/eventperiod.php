<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2010 by CANTICO ({@link http://www.cantico.fr})
 */

/**
 * Function registered to collect events.
 *
 * @param bab_eventBeforePeriodsCreated $event
 */
function caldav_onBeforePeriodsCreated(bab_eventBeforePeriodsCreated $event)
{


	if (!is_array($event->periods->calendars)) {
		return;
	}

	if (!$event->periods->isPeriodCollection('bab_CalendarEventCollection')) {
		return;
	}
	
	/* @var $backend Func_CalendarBackend_Caldav */
	$backend = bab_functionality::get('CalendarBackend/Caldav');
	
	$oneCalendar = false;
	foreach($event->periods->calendars as $calendar)
	{
		if ($calendar->getBackend() instanceof $backend)
		{
			$oneCalendar= true;
			break;
		}
	}

	
	if (!$oneCalendar)
	{
		return;
	}
	
	


	$criteria = $event->getCriteria();

	$beginDate = $event->getBeginDate();
	if ($beginDate instanceof BAB_DateTime) {
		$criteria = $criteria->_AND_(bab_PeriodCriteriaFactory::Begin($event->getBeginDate()));
	}
	$endDate = $event->getEndDate();
	if ($endDate instanceof BAB_DateTime) {
		$criteria = $criteria->_AND_(bab_PeriodCriteriaFactory::End($event->getEndDate()));
	}

	$periods = $backend->selectPeriods($criteria);

	foreach ($periods as $period) {
	//	bab_debug($period->toHtml(), DBG_TRACE, 'caldav');
		$event->periods->addPeriod($period);
	}
}





/**
 * Function registered to collect calendars.
 *
 * Add all calendars from caldav to the bab_icalendars collection
 *
 * @param bab_eventCollectCalendarsBeforeDisplay $event
 */
function caldav_onCollectCalendarsBeforeDisplay(bab_eventCollectCalendarsBeforeDisplay $event)
{
	global $babBody;

	/* @var $backend Func_CalendarBackend_Caldav */
	$backend = bab_functionality::get('CalendarBackend/Caldav');
	if (!isset($backend) || $backend === false) {
		return;
	}

	$personal_calendar = null;
	$access_user = $event->getAccessUser();
	$calendar_backend = $event->getBackendName();  // bab_getICalendars()->calendar_backend;

	if (!empty($access_user) && 'Caldav' === $calendar_backend) {

		try {
			$personal_calendar = $backend->PersonalCalendar($access_user);
		}
		catch(Exception $e)
		{
			bab_debug($e->getMessage());
		}

		if ($personal_calendar) {
			$personal_calendar->setName(bab_getUserName($access_user, true) . ' (CalDAV)');
			$personal_calendar->setAccessUser($access_user);
			$event->addCalendar($personal_calendar);
		}
	}


	if ('Caldav' !== $calendar_backend || $personal_calendar || $babBody->babsite['iPersonalCalendarAccess'] == 'Y') {
		$arr = $backend->getAccessiblePersonalCalendars($access_user, 'caldav_personal');

		foreach ($arr as $id_user) {
			$calendar = $backend->PersonalCalendar($id_user);
			$calendar->setName(bab_getUserName($id_user, true) . ' (CalDAV)');
			$calendar->setAccessUser($access_user);
			$event->addCalendar($calendar);
		}
	}
	
	// add resources calendars
	
	if (isset($GLOBALS['BAB_SESS_USERID']))
	{
		$_idu = $GLOBALS['BAB_SESS_USERID'];
	} else {
		// new addon controller
		$_idu = bab_getUserId();
	}
	
	
	if ($access_user == $_idu)
	{
		$arr = $backend->getAccessibleResourceCalendars();
		
		foreach ($arr as $uid => $resource) {
			$calendar = $backend->ResourceCalendar($uid, $resource);
			$calendar->setName($resource['name'] . ' (CalDAV)');
			$event->addCalendar($calendar);
		}
	}
}


