<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2010 by CANTICO ({@link http://www.cantico.fr})
 */
require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

/**
 * Save file in a temporary session folder
 */
function caldav_SessionFile($filename, $contents)
{
	return new caldav_SessionFile($filename, $contents);
}


/**
 * Save file in a temporary session folder
 */
class caldav_SessionFile
{
	static $cleanup = null;
	
	public function __construct($filename, $contents)
	{
		$this->cleanup();
		$this->saveFile($filename, $contents);
	}
	
	
	/**
	 * Cleanup session dir
	 * @return unknown_type
	 */
	private function cleanup()
	{
		if (isset(self::$cleanup))
		{
			return;
		}
		
		$sessions = array();
		foreach(bab_getActiveSessions() as $arr)
		{
			$sessions[$arr['session_id']] = 1;
		}
		
		$addon = bab_getAddonInfosInstance('LibCaldav');
		$session_folders = new bab_Path($addon->getUploadPath(), 'tmp');
		
		foreach($session_folders as $path)
		{
			if (!isset($sessions[$path->getBasename()]))
			{
				$path->deleteDir();
			}
		}
		
		self::$cleanup = true;
	}
	
	/**
	 * 
	 * @param string $filename
	 * @param string $contents
	 * @return int
	 */
	private function saveFile($filename, $contents)
	{
		
		$addon = bab_getAddonInfosInstance('LibCaldav');
		$path = new bab_Path($addon->getUploadPath(), 'tmp', session_id());
		$path->createDir();
		$path->push($filename);
		
		return file_put_contents($path->toString(), $contents);
	}
}