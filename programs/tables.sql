CREATE TABLE `libcaldav_resources` (
   `id` int(11) unsigned NOT NULL auto_increment,
   `name` varchar(64) NOT NULL default '',
   `url` varchar(255) NOT NULL default '',
   `nickname` varchar(255) NOT NULL default '',
   `password` varchar(255) NOT NULL default '',
   PRIMARY KEY  (`id`),
   KEY `name` (`name`)
);


CREATE TABLE `libcaldav_resource_groups` (
   `id` int(11) unsigned NOT NULL auto_increment,
   `id_object` int(11) unsigned NOT NULL DEFAULT '0',
   `id_group` int(11) unsigned NOT NULL DEFAULT '0',
   PRIMARY KEY (`id`),
   KEY `id_object` (`id_object`),
   KEY `id_group` (`id_group`)
);



CREATE TABLE `libcaldav_servers` (
   `id` int(11) unsigned NOT NULL auto_increment,
   `name` varchar(64) NOT NULL default '',
   `server_url` varchar(255) NOT NULL default '',
   `user_calendar_path` varchar(255) NOT NULL default '',
   `use_unique_id` varchar(5) NOT NULL default '',
   PRIMARY KEY  (`id`),
   KEY `name` (`name`)
);