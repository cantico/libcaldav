;<?php/*

[general]
name                        ="LibCaldav"
version                     ="0.4.18.2"
encoding                    ="UTF-8"
description                 ="CalDAV connector"
description.fr              ="Librairie partagée fournissant une API d'accès aux agendas partagés Caldav"
long_description.fr         ="README.md"
delete                      =1
db_prefix                   ="libcaldav_"
ov_version                  ="8.2.0"
php_version                 ="5.1.0"
addon_access_control        ="0"
author                      ="Laurent Choulette (laurent.choulette@cantico.fr)"
upload_directory            ="Available"
icon                        ="caldav.png"
mysql_character_set_database="latin1,utf8"
configuration_page          ="configuration"
tags                        ="library,calendar"

[addons]

widgets="1.0"

;*/?>