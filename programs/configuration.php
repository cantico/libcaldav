<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
require_once dirname(__FILE__) . '/functions.php';

function caldav_editConfiguration()
{
	global $babBody;
	$W = bab_Widgets();
	$page = $W->BabPage();
	$page->setTitle(caldav_translate('CalDAV configuration'));

	/* @var $caldav Func_CalendarBackend_Caldav */
	$caldav = bab_functionality::get('CalendarBackend/Caldav');

	if (!$caldav) {
		$babBody->addError(caldav_translate('Unable to instanciate \'CalendarBackend/Caldav\' functionality.'));
		$babBody->addError(caldav_translate('Please check that the LibCaldav addon is correctly installed.'));
		return;
	}

	$userCalendarUrl = $caldav->getDefaultUserCalendarUrl();
	$adminIdentifier =  $caldav->getAdminIdentifier();
	$adminPassword = $caldav->getAdminPassword();
	$storagebackend = $caldav->getStorageBackend();
	$verify_peer = $caldav->getVerifyPeer();
	$configbyuser = $caldav->configByUser();

	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/LibCaldav/');
	$default_backend = $registry->getValue('default_backend');
	
	$deleteOnCancel = $registry->getValue('deleteOnCancel', false);


	$form = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
	$options = $form->Options()->width(70,'em');
	$form->addClass('BabLoginMenuBackground')
		->addClass('widget-centered')
		->addClass('widget-bordered')
		->setCanvasOptions($options);
	$form->setHiddenValue('tg', bab_rp('tg'));
	$form->setHiddenValue('idx', 'saveConfiguration');
	
	$form->addItem(
		$W->LabelledWidget(caldav_translate('Use caldav for user events storage and allow users to select caldav'), $W->CheckBox()->setValue($storagebackend), 'storagebackend')
	);
	
	$common = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));
	
	
	
	$form->addItem(
			$W->LabelledWidget(caldav_translate('Users can configure the username and password'), $W->CheckBox()->setValue($configbyuser)->setAssociatedDisplayable($common, array('0')), 'configbyuser')
	);
	
	
	$form->addItem($common);
	
	$desc = caldav_translate('%u will be replaced by the ovidentia login id');
	
	$common->addItem($W->LabelledWidget(caldav_translate('User calendar url'), $W->LineEdit()->setSize(70)->setValue($userCalendarUrl), 'user_calendar_url', $desc));
	$common->addItem($W->LabelledWidget(caldav_translate('Admin identifier'), $W->LineEdit()->setSize(30)->setValue($adminIdentifier), 'admin_identifier'));
	$common->addItem($W->LabelledWidget(caldav_translate('Admin password'), $W->LineEdit()->setSize(30)->obfuscate()->setValue($adminPassword), 'admin_password'));
	$common->addItem($W->LabelledWidget(caldav_translate('Change calendar backend to caldav for all ovidentia users'), $W->CheckBox(), 'set_caldav'));
	$common->addItem($W->LabelledWidget(caldav_translate('Set the caldav backend for the new created users'), $W->CheckBox()->setValue((bool) $default_backend), 'default_backend'));
	
	
	$form->addItem($W->LabelledWidget(caldav_translate('SSL peer verification for HTTPS serveurs'), $W->CheckBox()->setValue((bool) $verify_peer), 'verify_peer'));
	
	
	$form->addItem($W->LabelledWidget(
	    caldav_translate('Delete events on cancel'), 
	    $W->CheckBox()->setValue($deleteOnCancel), 
	    'deleteOnCancel',
	    caldav_translate('On SoGo, canceled events are displayed like the others events, use this option to really delete events when canceled by ovidentia.')
	));
	
	$form->addItem($W->SubmitButton()->setLabel(caldav_translate('Save')));
	
	$page->addItem($form);
	
	$addon = bab_getAddonInfosInstance('LibCaldav');
	
	
	$page->addItemMenu('configuration', caldav_translate('Configuration'), $addon->getUrl().'configuration');
	$page->addItemMenu('servers', caldav_translate('Servers'), $addon->getUrl().'configuration&idx=servers');
	$page->addItemMenu('resources', caldav_translate('Resources'), $addon->getUrl().'configuration&idx=resources');
	
	$page->setCurrentItemMenu('configuration');
	
	$page->displayHtml();
}



function caldav_saveConfiguration($configbyuser, $userCalendarUrl, $adminIdentifier, $adminPassword, $storagebackend, $verify_peer, $set_caldav, $default_backend, $deleteOnCancel)
{
	$babBody = bab_getBody();

	/* @var $caldav Func_CalendarBackend_Caldav */
	$caldav = bab_functionality::get('CalendarBackend/Caldav');

	if (!$caldav) {
		$babBody->addError(caldav_translate('Unable to instanciate "CalendarBackend/Caldav" functionality.'));
		$babBody->addError(caldav_translate('Please check that the LibCaldav addon is correctly installed.'));
		return false;
	}
	
	if ($configbyuser)
	{
		$userCalendarUrl = '';
		$adminIdentifier = '';
		$adminPassword = '';
	}

	$caldav->setDefaultUserCalendarUrl($userCalendarUrl);
	$caldav->setAdminIdentifier($adminIdentifier);
	$caldav->setAdminPassword($adminPassword);
	$caldav->setStorageBackend($storagebackend);
	$caldav->setVerifyPeer($verify_peer);
	
	if ($set_caldav)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/calapi.php';
		
		if (!function_exists('bab_setPersonnalCalendarBackend'))
		{
			$babBody->addError(caldav_translate('Ovidentia 9.1.93 is required to set the caldendar backend on all users'));
		}
		
		$arr = bab_getGroupsMembers(BAB_REGISTERED_GROUP);
		foreach($arr as $user)
		{
			bab_setPersonnalCalendarBackend($user['id'], $caldav);
		}
	}
	
	
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/LibCaldav/');
	$registry->setKeyValue('default_backend', (bool) $default_backend);
	$registry->setKeyValue('deleteOnCancel', (bool) $deleteOnCancel);
	

	$babBody->babEcho('<div style="border: 1px solid #cca; padding: 1em; margin: 2em; background-color: #eec">' . caldav_translate('Configuration saved.') . '</div>');

	return true;
}




class caldav_servers
{
	private $res;

	public $t_name;
	public $t_url;
	public $t_add;
	public $addurl;
	
	public $altbg = true;


	public function __construct()
	{
		global $babDB;
		$addon = bab_getAddonInfosInstance('LibCaldav');

		$this->res = $babDB->db_query("SELECT * FROM libcaldav_servers");


		$this->t_name = caldav_translate('Name');
		$this->t_server_url = caldav_translate('Caldav Server Url');
		$this->t_user_calendar_path = caldav_translate('User calendar path');
		$this->t_add = caldav_translate('Add');
		$this->t_use_unique_id = caldav_translate('Use unique ID');

		$this->addurl = $addon->getUrl().'configuration&idx=server';

	}

	public function getnext()
	{
		global $babDB;

		if ($arr = $babDB->db_fetch_assoc($this->res))
		{
			$this->altbg = !$this->altbg;
			$addon = bab_getAddonInfosInstance('LibCaldav');

			$this->name = bab_toHtml($arr['name']);
			$this->server_url 	= bab_toHtml($arr['server_url']);
			$this->user_calendar_path 	= bab_toHtml($arr['user_calendar_path']);
			$this->use_unique_id = bab_toHtml(caldav_translate($arr['use_unique_id']));
			$this->editurl = bab_toHtml($addon->getUrl().'configuration&idx=server&server='.$arr['id']);

			return true;
		}
		return false;
	}

	public function display()
	{
		global $babBody;
		$addon = bab_getAddonInfosInstance('LibCaldav');

		$babBody->setTitle(caldav_translate('Availables servers for users configuration'));
		$babBody->babEcho(bab_printTemplate($this, $addon->getTemplatePath() . 'configuration.html', 'servers'));

		$babBody->addItemMenu('configuration', caldav_translate('Configuration'), $addon->getUrl().'configuration');
		$babBody->addItemMenu('servers', caldav_translate('Servers'), $addon->getUrl().'configuration&idx=servers');
		$babBody->addItemMenu('resources', caldav_translate('Resources'), $addon->getUrl().'configuration&idx=resources');

		$babBody->setCurrentItemMenu('servers');
	}
}







class caldav_resources
{
	private $res;
	
	public $t_name;
	public $t_url;
	public $t_add;
	public $addurl;
	
	public $altbg = true;
	
	
	public function __construct()
	{
		global $babDB;
		$addon = bab_getAddonInfosInstance('LibCaldav');
		
		$this->res = $babDB->db_query("SELECT * FROM libcaldav_resources");
		
		
		$this->t_name = caldav_translate('Name');
		$this->t_url = caldav_translate('Url');
		$this->t_add = caldav_translate('Add');
		$this->t_access = caldav_translate('Access');
		
		$this->addurl = $addon->getUrl().'configuration&idx=resource';
		
	}
	
	public function getnext()
	{
		global $babDB;
		
		if ($arr = $babDB->db_fetch_assoc($this->res))
		{
			$addon = bab_getAddonInfosInstance('LibCaldav');
			
			$this->altbg = !$this->altbg;
			
			$this->name = bab_toHtml($arr['name']);
			$this->url 	= bab_toHtml($arr['url']);
			$this->editurl = bab_toHtml($addon->getUrl().'configuration&idx=resource&resource='.$arr['id']);
			$this->accessurl = bab_toHtml($addon->getUrl().'configuration&idx=resourceaccess&id_resource='.$arr['id']);
			
			return true;
		}
		return false;
	}
	
	public function display()
	{
		global $babBody;
		$addon = bab_getAddonInfosInstance('LibCaldav');
		
		$babBody->setTitle(caldav_translate('Resources list'));
		$babBody->babEcho(bab_printTemplate($this, $addon->getTemplatePath() . 'configuration.html', 'resources'));
		
		$babBody->addItemMenu('configuration', caldav_translate('Configuration'), $addon->getUrl().'configuration');
		$babBody->addItemMenu('servers', caldav_translate('Servers'), $addon->getUrl().'configuration&idx=servers');
		$babBody->addItemMenu('resources', caldav_translate('Resources'), $addon->getUrl().'configuration&idx=resources');
		
		$babBody->setCurrentItemMenu('resources');
	}
}



class caldav_server_editor
{
	public function __construct()
	{
		global $babDB;
		
		$this->t_name = caldav_translate('Name');
		$this->t_server_url = caldav_translate('Caldav Server Url');
		$this->t_server_url_help = caldav_translate('Example: http://localhost/davical/caldav.php');
		$this->t_user_calendar_path = caldav_translate('User calendar path');
		$this->t_user_calendar_path_help = caldav_translate('%u will be replaced by the login id, example: /%u/home');
		$this->t_user_calendar_path_help2 = caldav_translate('if the login id contain an email address %n will be replaced by the username before @, otherwise it will be replaced by the full login id, example: /%n/home');
		$this->t_user_calendar_path_help3 = caldav_translate('If a unique id is needed, %c will be replaced by the unique id of the user, example : %c/ will give  https://url.yourservercaldav.com/calendars/%c/');
		$this->t_need_unique_id = caldav_translate('Needs a unique id');
		$this->t_need_unique_id_help = caldav_translate("It means that the user will have to give his id which is contain in the Url of the calendar, example: https://dav.fruux.com/calendars/XXXXXXXXXXX/XXXXXXXXXXX/");
		$this->t_record = caldav_translate('Save');
		$this->t_delete = caldav_translate('Delete');
		$this->t_confirm = caldav_translate('Do you really want to delete this server configuration?');
		
		$id = bab_rp('server');
		if (!empty($id))
		{
			$res = $babDB->db_query("SELECT * FROM libcaldav_servers WHERE id=".$babDB->quote($id));
			$arr = $babDB->db_fetch_assoc($res);
		
			if (!$arr)
			{
				throw new Exception('server not found');
			}
		
			$this->server = bab_toHtml($arr['id']);
			$this->name = bab_toHtml($arr['name']);
			$this->test = bab_toHtml("mdr");
			$this->server_url = bab_toHtml($arr['server_url']);
			$this->user_calendar_path = bab_toHtml($arr['user_calendar_path']);
			$this->use_unique_id = bab_toHtml(" ");
			if($arr["use_unique_id"]=="true")
			{
			    $this->use_unique_id = bab_toHtml("checked");
			}
		
		} else {
			$this->server = '';
			$this->name = '';
			$this->server_url = '';
			$this->user_calendar_path = '';
			$this->use_unique_id='';
		}
		
	}
	
	public function display()
	{
		global $babBody;
		$addon = bab_getAddonInfosInstance('LibCaldav');
	
		$babBody->setTitle(caldav_translate('Edit server'));
		$babBody->babEcho(bab_printTemplate($this, $addon->getTemplatePath() . 'configuration.html', 'editserver'));
	
	}
}

class caldav_resource_editor
{
	public function __construct()
	{
		global $babDB;
		$addon = bab_getAddonInfosInstance('LibCaldav');
	
		
	
	
		$this->t_name = caldav_translate('Name');
		$this->t_name_help = caldav_translate('Name of calendar as displayed in the selection list');
		$this->t_url = caldav_translate('Url');
		$this->t_url_help = caldav_translate('Full url to the caldav calendar (ex: http://serveur/davical/caldav.php/user/home/)');
		$this->t_nickname = caldav_translate('Nickname');
		$this->t_password = caldav_translate('Password');
		$this->t_record = caldav_translate('Save');
		$this->t_delete = caldav_translate('Delete');
		$this->t_confirm = caldav_translate('Do you really want to delete this resource?');
	
		$this->addurl = $addon->getUrl().'configuration&idx=resource';
		
		
		$id = bab_rp('resource');
		if (!empty($id))
		{
			$res = $babDB->db_query("SELECT * FROM libcaldav_resources WHERE id=".$babDB->quote($id));
			$arr = $babDB->db_fetch_assoc($res);
			
			if (!$arr)
			{
				throw new Exception('resource not found');
			}
			
			$this->resource = bab_toHtml($arr['id']);
			$this->name = bab_toHtml($arr['name']);
			$this->url = bab_toHtml($arr['url']);
			$this->nickname = bab_toHtml($arr['nickname']);
			$this->password = bab_toHtml($arr['password']);
			
		} else {
			$this->resource = '';
			$this->name = '';
			$this->url = '';
			$this->nickname = '';
			$this->password = '';
		}
		
		
	
	}
	
	
	public function display()
	{
		$babBody = bab_getBody();
		$addon = bab_getAddonInfosInstance('LibCaldav');
	
		$babBody->setTitle(caldav_translate('Resource'));
		$babBody->babEcho(bab_printTemplate($this, $addon->getTemplatePath() . 'configuration.html', 'edit'));

	}
}



function caldav_deleteServer()
{
	require_once $GLOBALS['babInstallPath'].'/utilit/urlincl.php';
	global $babDB, $babBody;
	$id_server = bab_pp('id_server', null);

	if (empty($id_server))
	{
		return;
	}
	
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/LibCaldav/Users/');
	$keys = array();
	while ($id_user = $registry->fetchChildDir())
	{
		$keys[] = $id_user.'server';
	}
	

	$res = $registry->getValueEx($keys);
	/*@var $res bab_RegistryIterator */
	
	foreach($res as $arr)
	{
		if (((int) $id_server) === (int) $arr['value'])
		{
			$babBody->addError(sprintf(caldav_translate('A user is associated to this server')));
			return false;
		}
	}
	
		
	$babDB->db_query('DELETE FROM libcaldav_servers WHERE id='.$babDB->quote($id_server));
	

	$url = bab_url::get_request('tg');
	$url->idx = 'servers';
	$url->location();
}



function caldav_deleteResource()
{
	require_once $GLOBALS['babInstallPath'].'/utilit/urlincl.php';
	global $babDB;
	$id_resource = bab_pp('id_resource', null);
	
	if (!empty($id_resource))
	{
		$babDB->db_query('DELETE FROM libcaldav_resources WHERE id='.$babDB->quote($id_resource));
	}
	
	$url = bab_url::get_request('tg');
	$url->idx = 'resources';
	$url->location();
}





function caldav_saveServer()
{
	require_once $GLOBALS['babInstallPath'].'/utilit/urlincl.php';
	global $babDB, $babBody;

	$id_server = bab_pp('id_server', null);
	$name = bab_pp('name');
	$server_url = bab_pp('server_url');
	$user_calendar_path = bab_pp('user_calendar_path');
	$use_unique_id = bab_pp('use_unique_id');


	if (empty($name))
	{
		$babBody->addError(caldav_translate('The name is mandatory'));
		return false;
	}

	if (empty($server_url))
	{
		$babBody->addError(caldav_translate('The server url is mandatory'));
		return false;
	}
	
	if (empty($user_calendar_path))
	{
		$babBody->addError(caldav_translate('The user calendar path is mandatory'));
		return false;
	}
	
	if(empty($use_unique_id))
	{
	    $use_unique_id = "false";
	}
	;


	if (!empty($id_server))
	{
		$babDB->db_query('
			UPDATE libcaldav_servers SET
				name='.$babDB->quote($name).',
				server_url='.$babDB->quote($server_url).',
				user_calendar_path='.$babDB->quote($user_calendar_path).',
                use_unique_id='.$babDB->quote($use_unique_id).' 
			WHERE id='.$babDB->quote($id_server)
		);


	} else {

		$babDB->db_query('
			INSERT INTO libcaldav_servers
				(name, server_url, user_calendar_path,use_unique_id)
			VALUES
				(
				'.$babDB->quote($name).',
				'.$babDB->quote($server_url).',
				'.$babDB->quote($user_calendar_path).',
                '.$babDB->quote($use_unique_id).'    
				)'
		);

	}


	$url = bab_url::get_request('tg');
	$url->idx = 'servers';
	$url->location();
}




function caldav_saveResource()
{
	require_once $GLOBALS['babInstallPath'].'/utilit/urlincl.php';
	global $babDB, $babBody;
	
	$id_resource = bab_pp('id_resource', null);
	$name = bab_pp('name');
	$url = bab_pp('url');
	$nickname = bab_pp('nickname');
	$password = bab_pp('password');


	if (empty($name))
	{
		$babBody->addError(caldav_translate('The name is mandatory'));
		return false;
	}
	
	if (empty($url))
	{
		$babBody->addError(caldav_translate('The url is mandatory'));
		return false;
	}
	
	
	if (!empty($id_resource))
	{
		$babDB->db_query('
			UPDATE libcaldav_resources SET 
				name='.$babDB->quote($name).',
				url='.$babDB->quote($url).',
				nickname='.$babDB->quote($nickname).',
				password='.$babDB->quote($password).' 
			WHERE id='.$babDB->quote($id_resource)
		);
		
		
	} else {
		
		$babDB->db_query('
				INSERT INTO libcaldav_resources 
					(name, url, nickname, password) 
				VALUES 
					(
						'.$babDB->quote($name).', 
						'.$babDB->quote($url).',
						'.$babDB->quote($nickname).',
						'.$babDB->quote($password).'
					)'
		);
		
	}
	
	
	$url = bab_url::get_request('tg');
	$url->idx = 'resources';
	$url->location();
}


if (!bab_isUserAdministrator())
{
	return;
}


$idx = bab_rp('idx');

switch($idx) {
	
	case 'saveResource':
		if (isset($_POST['delete']))
		{
			caldav_deleteResource();
		}
		caldav_saveResource();
	
	case 'resource':
		$resources = new caldav_resource_editor();
		$resources->display();
		break;
	
	case 'resources':
		$resources = new caldav_resources();
		$resources->display();
		break;
		
	case 'resourceaccess':
		require_once $GLOBALS['babInstallPath'].'admin/acl.php';
		require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
		
		if (bab_rp('aclview'))
		{
			maclGroups();
			$url = bab_url::get_request('tg');
			$url->idx = 'resources';
			$url->location();
		}
		
		$macl = new macl(bab_rp('tg'), bab_rp('idx'), bab_rp('id_resource'), 'aclview');
		$macl->addtable('libcaldav_resource_groups', caldav_translate('Who can view this resource?'));
		$macl->babecho();
		break;
	

	case 'saveConfiguration':
		caldav_saveConfiguration(
			(bool) bab_rp('configbyuser'), 
			bab_rp('user_calendar_url'), 
			bab_rp('admin_identifier'), 
			bab_rp('admin_password'), 
			bab_rp('storagebackend'), 
			(bool) bab_rp('verify_peer'),
			(bool) bab_rp('set_caldav'),
			(bool) bab_rp('default_backend'),
			(bool) bab_rp('deleteOnCancel')
		);
		caldav_editConfiguration();
		break;
		
	case 'servers':
		$servers = new caldav_servers();
		$servers->display();
		break;
	
	case 'saveServer':
		if (isset($_POST['delete']))
		{
			if (!caldav_deleteServer())
			{
				$servers = new caldav_servers();
				$servers->display();
			}
		} else {
			caldav_saveServer();
		}
		break;
		
	case 'server':
		$server = new caldav_server_editor();
		$server->display();
		break;

	default:
	case 'configuration':
		caldav_editConfiguration();
		break;
}
