<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


/**
 * Translates the string.
 *
 * @param string $str
 * @return string
 */
function caldav_translate($str)
{
	return bab_translate($str, 'LibCaldav');
}



/**
 * Parse attendee property
 * @param string $iCalPropertyName
 * @return array
 */
function caldav_getAttendeeProp($iCalPropertyName, $iCalPropertyValue)
{
    $parameters = explode(';', $iCalPropertyName);
    array_shift($parameters);
     
    $role = null;
    $partstat = null;
    $cn = '';
    $email = null;
     
    foreach ($parameters as $parameter) {
        list($paramName, $paramValue) = explode('=', $parameter);
        switch ($paramName) {
            case 'ROLE':
                $role = $paramValue;
                break;
            case 'PARTSTAT':
                $partstat = $paramValue;
                break;
            case 'CN':
                $cn = trim($paramValue, '" ');
                break;
        }
    }
    if (stripos($iCalPropertyValue, 'MAILTO:') !== false) {
        list(, $email) = explode(':', $iCalPropertyValue);
    }
     
    return array($role, $partstat, $cn, $email);
}