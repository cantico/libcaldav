<?php
/* $Id$ */

    require_once 'awl/iCalendar.php';
    require_once 'caldavresource.class.php';
    require_once 'icomponent.class.php';
    require_once 'rruleparser.class.php';

    class VEvent extends IComponent {

        private $rulesParser;

        function __construct($etag, $url, VTYPE $type, iCalendar $item, $new) {
            parent::__construct($etag, $url, $type, $item, $new);
            $this->rulesParser = new RRuleParser();
        }

        function isActive($start, $end) {
            $res = FALSE;
            if (!($start && $end))
                return TRUE;
            if (! CaldavRessource::isDateTime($start) ||
                ! CaldavRessource::isDateTime($end))
                throw new Exception(
                    "[$start,$end] Invalid CalDAV DateTime format");
            $event = $this->getBaseComponent();
            if ($start && !$end) {
                if (CaldavRessource::datecmp(
                    $start, $event->GetPValue('DTSTART')) < 0)
                    $res = TRUE;
            }
            else {
                if (CaldavRessource::datecmp(
                        $start, $event->GetPValue('DTSTART')) < 0 &&
                    CaldavRessource::datecmp(
                        $end, $event->GetPValue('DTEND')) > 0)
                    $res = TRUE;
            }
            return $res;
        }

        function getActiveDates($range_start = NULL, $range_end = NULL) {
            $res = array();
            $event = $this->getBaseComponent();
            //print_r($event);
            $start = $event->GetPValue('DTSTART');
            $end = $event->GetPValue('DTEND');
            //print "$start:$end<br/>";
            if (! ($start && $end))
                return $res;
            $rrule = $event->GetPValue('RRULE');
            if ($rrule) {
                $this->rulesParser->setRule($rrule, $start, $end);
                //print $this->rulesParser->__toString()."\n";
                $res = $this->rulesParser->getEventDates(
                                    $range_start, $range_end);
                //print_r($res);
            }
            else {
                if ($this->isActive($range_start, $range_end))
                    array_push($res, $start);
            }
            //var_dump($res);
            return $res;
        }

        function getRRule() {
            return $this->rulesParser;
        }

        function getAlarm() {
            $alarm = $this->getComponent(VTYPE::VALARM);
//            print_r($alarm);
            if ($alarm)
                $alarm = $alarm[0];
            return $alarm;
        }

    }
?>
