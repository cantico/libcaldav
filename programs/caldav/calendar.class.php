<?php
/* $Id$ */

    require_once 'caldav-client.php';
    require_once 'awl/iCalendar.php';
    require_once 'caldavresource.class.php';
    require_once 'rruleparser.class.php';
    require_once 'vevent.class.php';
    require_once 'icomponent.class.php';

    class CalendarIterator implements Iterator {
        private $list;

        function __construct(array $list) {
            $this->list = $list;
        }

        function current() {
            return current($this->list);
        }

        function next() {
            next($this->list);
        }

        function key() {
            return key($this->list);
        }

        function rewind() {
            reset($this->list);
        }

        function valid() {
            $obj = current($this->list);
            return ($obj !== FALSE);
        }

    }

    class Calendar extends CaldavRessource {

        private $calendar;

	function __construct($url, $uid = '', $pwd = '', $cal = '') {
	    //file_put_contents('/tmp/dump', "$url\n$uid\n$pwd\n$cal\n", FILE_APPEND);
            if (empty($url))
                throw new Exception("Missing URL");
            parent::__construct($url, $uid, $pwd, $cal);
        }

        private function setComponent(VTYPE $type, array $item, $new = FALSE) {
            switch ($type) {
                case VTYPE::VEVENT:
                    $ical = new VEvent(
                        $item['etag'], $item['href'],
                        $type, $item['ical'], $new);
                    break;
                default:
                    throw new Exception(
                        "$thisType: Unsupported iCalendar component");
            }
            $this->calendar[$item['etag']] = $ical;
        }

        private function setResource($etag, $resource) {
            if ($resource === NULL)
                unset($this->calendar[$etag]);
            else if (isset($this->calendar[$etag]))
                $this->calendar[$etag]->setResource($resource);
            else {
                $type = new VTYPE($this->getType($resource));
                $this->setComponent($type, array(
                    'etag' => $etag,
                    'href' => NULL,
                    'ical' => $resource),
                    TRUE
                );
            }
        }

        private function getType(iCalendar $iCalendar) {
            $components = $iCalendar->component->GetComponents();
            // Find VCalender component
            foreach($components as $type) {
                try {
                    $vtype = new VTYPE($type->GetType());
                    if ($vtype->ordinal() != VTYPE::VTIMEZONE)
                        break;
                }
                catch (Exception $ex) {}
            }
            return $vtype;
        }

        function getComponents($start, $end) {
            $this->calendar = array();

            if (! $this->isDateTime($start) || ! $this->isDateTime($end))
                throw new Exception("[$start:$end]: Invalid DateTime format");
            //print "$start:$end<br/>";
            //file_put_contents('/tmp/dump', "$start, $end\n", FILE_APPEND);
            $events = $this->callServer('getEvents', array($start, $end));
            //var_export($events, FALSE);
            //file_put_contents('/tmp/dump', var_export($events, TRUE), FILE_APPEND);
            foreach ($events as $k => $event) {
                $iCalendar = new iCalendar(
                    array('icalendar' => $event['data']));
                $vtype = $this->getType($iCalendar);
                $this->setComponent($vtype, array(
                    'etag' => $event['etag'],
                    'href' => $event['href'],
                    'ical' => $iCalendar
                    )
                );
            }
        }

        function update() {
            $doUpdate;

            foreach($this->calendar as $etag => $resource) {
                $doUpdate = FALSE;
                if ($resource->isDirty())
                    $doUpdate = TRUE;
                if ($doUpdate) {
                    // update (call put)
                    print "$etag: update\n";
                }
                else {
                    print "$etag: no update\n";
                }
            }
        }

        // inherited abstract methods from parent
        function offsetExists($etag) {
            return (is_object($this->calendar[$etag]) &&
                $this->calendar[$etag] instanceof IComponent);
        }

        function offsetGet($etag) {
            if ($this->offsetExists($etag))
                return $this->calendar[$etag]->getResource();
        }

        function offsetSet($etag, $ical) {
            $this->setResource($etag, $ical);
        }

        function offsetUnset($etag) {
            $this->setResource($etag, NULL);
        }

        function getIterator() {
            return new CalendarIterator($this->calendar);
        }

    }
/*
$cal = new Calendar(
    'http://calendar.datanom.net/caldav.php/mir/home/',
    'uid',
    'pwd'
);
$cal->getComponents("20030830T000000Z","20031201T000000Z");
//print_r($cal);
$i = 0;
foreach($cal as $obj) {
    $i++;
    print "========= [$i] =========\n";
    //print_r($obj);
    //print_r ($obj->getAlarm());
    print_r($obj->getActiveDates("20031014T000000Z","20031114T000000Z"));
    //print "{$obj->isUTCTime()}\n";
    //$obj->getActiveDates();
}
print "Found $i event(s)\n";

//print_r ($cal->getUrlByEtag($cal->getEtagFromUid('KOrganizer-1670268771.406')));
$time = time();
print "time: $time\n";
$dt = $cal->timestamp2ICal($time, TRUE);
print "dt: $dt\n";
$time = $cal->iCal2Timestamp($dt);
print "time: $time\n";
$dt = $cal->timestamp2ICal($time, FALSE);
print "dt: $dt\n";
$time = $cal->iCal2Timestamp(substr($dt, 0, strpos($dt, 'T')));
$dt = $cal->timestamp2ICal($time, TRUE);
print "dt: $dt\n";
$r = new RRuleParser(
    'FREQ=HOURLY;INTERVAL=3;UNTIL=20070101T170000Z',
    '20070101T090000Z', '20070101T090000Z');
$r = new RRuleParser(
    'FREQ=WEEKLY;COUNT=12;INTERVAL=2',
    '20070101T140000Z', '20070101T120000Z');
print "$r\n";
print_r($r->getEventDates('20070301T140000Z','20070501T140000Z'));
$r = new RRuleParser(
    'FREQ=MONTHLY;BYDAY=MO,TU,WE,TH,FR;BYSETPOS=-1',
    '20070101T000100Z', '20070101T001100Z');
//DTSTART;TZID=US-Eastern:19970105T083000
print "$r\n";
$r = new RRuleParser(
    'FREQ=YEARLY;INTERVAL=2;BYMONTH=1;BYDAY=SU;BYHOUR=8,9;BYMINUTE=30',
    '20070101T000100Z', '20070101T001100Z');
print "$r\n";
print_r ($r->getEventDates('20060101T000100Z', '20060101T001100Z'));
$r = new RRuleParser(
    'FREQ=DAILY;COUNT=10;INTERVAL=2',
    '20070101T000100Z', '20070101T001100Z');
print "$r\n";
//foreach ($cal as $obj)
//    var_dump($obj->getBaseComponent());
//$bak = $cal['3ba46312e910765bf7059a53909d149b'];
//print_r($bak);
//print_r(new Icalendar(array('SUMMARY' => 'test')));
//$cal['3ba46312e910765bf7059a53909d149b'] = new Icalendar(array('SUMMARY' => 'test'));
//print_r($cal['3ba46312e910765bf7059a53909d149b']);
//unset($cal['3ba46312e910765bf7059a53909d149b']);
//var_dump($cal['3ba46312e910765bf7059a53909d149b']);
//$cal['3ba46312e910765bf7059a53909d149b'] = $bak;
//var_dump($cal['3ba46312e910765bf7059a53909d149b']);
//$cal->update();
//print_r($cal['3ba46312e910765bf7059a53909d149b']);*/
?>
