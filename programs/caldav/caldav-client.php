<?php
/**
 * A Class for connecting to a caldav server
 *
 * @package   awl
 * removed curl - now using fsockopen
 * changed 2009 by Andres Obrero - Switzerland andres@obrero.ch
 *
 * @subpackage   caldav
 * @author Andrew McMillan <debian@mcmillan.net.nz>
 * @copyright Andrew McMillan
 * @license   http://gnu.org/copyleft/gpl.html GNU GPL v2
 */


if (!function_exists('http-chunked-decode')) {
	/**
	 * dechunk an http 'transfer-encoding: chunked' message
	 *
	 * @param string $chunk the encoded message
	 * @return string the decoded message.  If $chunk wasn't encoded properly it will be returned unmodified.
	 */
	function http_chunked_decode($chunk) {
		$pos = 0;
		$len = strlen($chunk);
		$dechunk = null;

		while(($pos < $len)
		&& ($chunkLenHex = substr($chunk,$pos, ($newlineAt = strpos($chunk,"\n",$pos+1))-$pos)))
		{
			if (! is_hex($chunkLenHex)) {
				trigger_error('Value is not properly chunk encoded', E_USER_WARNING);
				return $chunk;
			}

			$pos = $newlineAt + 1;
			$chunkLen = hexdec(rtrim($chunkLenHex,"\r\n"));
			$dechunk .= substr($chunk, $pos, $chunkLen);
			$pos = strpos($chunk, "\n", $pos + $chunkLen) + 1;
		}
		return $dechunk;
	}
}

/**
 * determine if a string can represent a number in hexadecimal
 *
 * @param string $hex
 * @return boolean true if the string is a hex, otherwise false
 */
function is_hex($hex) {
	// regex is for weenies
	$hex = strtolower(trim(ltrim($hex,"0")));
	if (empty($hex)) {
		$hex = 0;
	};
	$dec = hexdec($hex);
	return ($hex == dechex($dec));
}



/**
 * Wrong credential on caldav account
 */
class caldav_AccessException extends Exception {}

class caldav_HttpException extends Exception {}


/**
 * A class for accessing DAViCal via CalDAV, as a client
 *
 * @package   awl
 */
class CalDAVClient {
	/**
	 * Server, username, password, calendar
	 *
	 * @var string
	 */
	var $base_url, $user, $pass, $calendar, $entry, $protocol, $server, $port;

	/**
	 * The useragent which is send to the caldav server
	 *
	 * @var string
	 */
	var $user_agent = 'DAViCalClient';

	var $headers = array();
	var $body = "";
	var $requestMethod = "GET";
	var $httpRequest = ""; // for debugging http headers sent
	var $xmlRequest = ""; // for debugging xml sent
	var $httpResponse = ""; // for debugging http headers received
	var $xmlResponse = ""; // for debugging xml received


    var $verify_peer = true;


	/**
	 * Constructor, initialises the class
	 *
	 * @param string $base_url  The URL for the calendar server
	 * @param string $user      The name of the user logging in
	 * @param string $pass      The password for that user
	 * @param string $calendar  The name of the calendar (not currently used)
	 */
	function CalDAVClient($base_url, $user, $pass, $calendar)
	{
		$this->user = $user;
		$this->pass = $pass;
		$this->calendar = $calendar;
		$this->headers = array();

		if ( preg_match( '#^(https?)://([a-z0-9.-]+)(:([0-9]+))?(/.*)$#', $base_url, $matches ) ) {
			$this->server = $matches[2];
			$this->base_url = $matches[5];
			if ( $matches[1] == 'https' ) {
				$this->protocol = 'ssl';
				$this->port = 443;
			}
			else {
				$this->protocol = 'tcp';
				$this->port = 80;
			}
			if ( $matches[4] != '' ) {
				$this->port = intval($matches[4]);
			}
		}
		else {
			trigger_error("Invalid URL: '".$base_url."'", E_USER_ERROR);
		}
		
		/* @var $caldav Func_CalendarBackend_Caldav */
		$caldav = bab_functionality::get('CalendarBackend/Caldav');
		$this->verify_peer = $caldav->getVerifyPeer();
	}

	/**
	 * Adds an If-Match or If-None-Match header
	 *
	 * @param bool $match to Match or Not to Match, that is the question!
	 * @param string $etag The etag to match / not match against.
	 */
	function SetMatch( $match, $etag = '*' ) {
		$this->headers[] = sprintf( "%s-Match: %s", ($match ? "If" : "If-None"), $etag);
	}

	/**
	 * Add a Depth: header.  Valid values are 1 or infinity
	 *
	 * @param int $depth  The depth, default to infinity
	 */
	function SetDepth( $depth = 'infinity' ) {
		$this->headers[] = 'Depth: '. $depth;
	}

	/**
	 * Add a Depth: header.  Valid values are 1 or infinity
	 *
	 * @param int $depth  The depth, default to infinity
	 */
	function SetUserAgent( $user_agent = null ) {
		if ( !isset($user_agent) ) $user_agent = $this->user_agent;
		$this->user_agent = $user_agent;
	}

	/**
	 * Add a Content-type: header.
	 *
	 * @param int $type  The content type
	 */
	function SetContentType( $type ) {
		$this->headers[] = "Content-type: $type";
	}

	/**
	 * Split response into httpResponse and xmlResponse
	 *
	 * @param string Response from server
	 */
	function ParseResponse($rawHttp)
	{
		$headerEndPos = strpos($rawHttp, "\r\n\r\n");

		if ($headerEndPos !== false) {
			$header = substr($rawHttp, 0, $headerEndPos);
			$content = substr($rawHttp, $headerEndPos + 4);
			$this->httpResponse = $header;

			// Some Caldav servers (Kerio) send chunked-encoded http responses.
			if (stripos($header, 'Transfer-Encoding: chunked') !== false) {
				$this->xmlResponse = http_chunked_decode($content);
			} else {
				$this->xmlResponse = $content;
			}
		} else {
			$this->httpResponse = trim($rawHttp);
			$this->xmlResponse = '';
		}

/*
		$pos = strpos($rawHttp, '<?xml');
		if ($pos == false) {
			$this->httpResponse = trim($rawHttp);
		} else {
			$this->httpResponse = trim(substr($rawHttp, 0, $pos));
			$this->xmlResponse = trim(substr($rawHttp, $pos));
		}
*/
	}

	/**
	 * Output http request headers
	 *
	 * @return HTTP headers
	 */
	function GetHttpRequest() {
		return $this->httpRequest;
	}
	/**
	 * Output http response headers
	 *
	 * @return HTTP headers
	 */
	function GetHttpResponse() {
		return $this->httpResponse;
	}
	/**
	 * Output xml request
	 *
	 * @return raw xml
	 */
	function GetXmlRequest() {
		return $this->xmlRequest;
	}
	/**
	 * Output xml response
	 *
	 * @return raw xml
	 */
	function GetXmlResponse() {
		return $this->xmlResponse;
	}

	/**
	 * this key change for each user configuration
	 * this must contain the password because if an account is configured for two different user, one must have a bad password and make 401 errors
	 * @return string
	 */
	private function getFailureKey($relative_url = '')
	{
		return $this->user .':'. $this->pass.'@'.$this->server.':'.$this->port.$this->base_url . $relative_url;
	}



// 	/**
// 	 * Unchunk a chunked http response.
// 	 * Does nothing if http response was not chunk-encoded.
// 	 *
// 	 * @param string $httpResponse A raw http response
// 	 * @return string The corresponding raw response unchunked if necessary.
// 	 */
// 	private function chunkedDecode($rawHttp)
// 	{
// 		$headerEndPos = strpos($rawHttp, "\r\n\r\n");
// 		if ($headerEndPos !== false) {
// 			$header = substr($rawHttp, 0, $headerEndPos);
// 			$this->httpResponse = $header;
// 			if (stripos($header, 'Transfer-Encoding: chunked') !== false) {
// 				$content = substr($rawHttp, $headerEndPos + 4);
// 				$rawHttp = $header . http_chunked_decode($content);
// 			} else {
// 				$this->xmlResponse = http_chunked_decode($content);
// 			}
// 		}

// 		$this->httpResponse = trim(substr($response, 0, $pos));
// 		$this->xmlResponse = trim(substr($response, $pos));

// 		return $httpResponse;
// 	}


	/**
	 * Send a request to the server
	 *
	 * @param string $relative_url The URL to make the request to, relative to $base_url
	 *
	 * @return string The content of the response from the server
	 */
	function DoRequest($relative_url = '')
	{
		static $failed_url = array();
		
		$url = $this->base_url;
		if ('/' !== substr($url,-1))
		{
			$url .= '/';
		}
		$url .= $relative_url;

		if (isset($failed_url[$this->getFailureKey($relative_url)]))
		{
			bab_debug('ignored query to '.$url.' because previous query fail : '.$failed_url[$this->getFailureKey($relative_url)], 2, 'caldav');
			return false;
		}

		if (!defined('_FSOCK_TIMEOUT')) {
			define('_FSOCK_TIMEOUT', 2);
		}
		$headers = array();
		
		

		$headers[] = $this->requestMethod.' '. $url . ' HTTP/1.1';
		$headers[] = 'Authorization: Basic '.base64_encode($this->user .':'. $this->pass );
		$headers[] = 'Host: '.$this->server ; //.':'.$this->port;

		foreach ($this->headers as $head) {
			$headers[] = $head;
		}
		$headers[] = "Content-Length: " . strlen($this->body);
		$headers[] = "User-Agent: " . $this->user_agent;
		$headers[] = 'Connection: close';
		$this->httpRequest = join("\r\n",$headers);
		$this->xmlRequest = $this->body;

		$time_uid = uniqid();
		$this->time_start($time_uid, $url);
		
		$context = stream_context_create();
		stream_context_set_option($context, "ssl", "allow_self_signed", true);
		stream_context_set_option($context, "ssl", "verify_peer", $this->verify_peer);
		
		$errno = null;
		$errstr = null;
		
		$target = $this->protocol . '://' . $this->server.':'.$this->port;
		
		$fip = stream_socket_client($target, $errno, $errstr, _FSOCK_TIMEOUT, STREAM_CLIENT_CONNECT, $context);

		
		if (!$fip || get_resource_type($fip) !== 'stream') {
			$failed_url[$this->getFailureKey($relative_url)] = $errstr;
			$this->time_end($time_uid);
			
			if (!$errstr) {
			    // error before socket initialization, see warnings in error log
			    throw new Exception('Stream creation failed to '.$target.' This can be an SSL verification error');
			}
			
			throw new Exception($errstr, $errno);
		}

		$debug = "<h3>Request: </h3><div class=\"bab_dbg_call_info\">".$this->httpRequest."\r\n\r\n".bab_toHtml($this->body)."</div>";
		
		
		if (!fwrite($fip, $this->httpRequest."\r\n\r\n".$this->body)) {
			fclose($fip);
			$failed_url[$this->getFailureKey($relative_url)] = 'fwrite failed';
			bab_debug(bab_getStringAccordingToDataBase($debug, 'UTF-8'), 2, 'caldav');
			$this->time_end($time_uid);
			throw new Exception('Error while trying to write to server.');
		}

		// get http error code
		$rsp = fgets($fip, 16);



		if (preg_match('/^HTTP\/[\.\d]+ 401/', $rsp))
		{
			// unauthorized access to calendar
			$failed_url[$this->getFailureKey($relative_url)] = 'HTTP 401';
			bab_debug(bab_getStringAccordingToDataBase($debug, 'UTF-8'), 2, 'caldav');
			$this->time_end($time_uid);
			throw new caldav_AccessException(sprintf(caldav_translate('Unauthorized access, wrong login or password in CalDav account : %s'), $this->user));
		}
		
		
		while (!feof($fip)) {
		    $rsp .= fgets($fip, 8192);
		}
		fclose($fip);
		
		
		if (preg_match('/^HTTP\/[\.\d]+\s(40\d)/', $rsp, $http))
		{
		    if(!preg_match('/^HTTP\/[\.\d]+\s(404)/', $rsp, $http))
		    {
		        $failed_url[$this->getFailureKey($relative_url)] = 'HTTP '.$http[1];
		        $this->time_end($time_uid);
		        
		        if (preg_match('/<body.*\/body>/s', $rsp, $m)) {
		            $body = strip_tags(bab_getStringAccordingToDataBase($m[0], 'UTF-8'));
		        } else {
		            $body = $http[1].' HTTP error';
		        }
		        
		        bab_debug(bab_getStringAccordingToDataBase($debug, 'UTF-8'), 2, 'caldav');
		        throw new caldav_HttpException($body);
		    }
		}
		


		

		$this->time_end($time_uid);




		$this->headers = array();  // reset the headers array for our next request
		$this->ParseResponse($rsp);

		$debug .= "<h3>Response: </h3><div class=\"bab_dbg_call_info\">".bab_toHtml($rsp)."</div>";

		bab_debug(bab_getStringAccordingToDataBase($debug, 'UTF-8'), 2, 'caldav');
		return $rsp;
	}



	private function time_start($uid, $comment)
	{
		$_SESSION['LibCaldav']['stats']['pending'][$uid] = array('start' => microtime(true), 'calendar' => $comment);
	}


	private function time_end($uid)
	{
		if (!isset($_SESSION['LibCaldav']['stats']['pending'][$uid]))
		{
			return;
		}

		$start = $_SESSION['LibCaldav']['stats']['pending'][$uid]['start'];
		unset($_SESSION['LibCaldav']['stats']['pending'][$uid]['start']);
		$end = microtime(true);
		$_SESSION['LibCaldav']['stats']['pending'][$uid]['duration'] = ($end - $start);
	}


	/**
	 * Send an OPTIONS request to the server
	 *
	 * @param string $relative_url The URL to make the request to, relative to $base_url
	 *
	 * @return array The allowed options
	 */
	function DoOptionsRequest( $relative_url = "" ) {
		$this->requestMethod = "OPTIONS";
		$this->body = "";
		$headers = $this->DoRequest($relative_url);
		$options_header = preg_replace( '/^.*Allow: ([a-z, ]+)\r?\n.*/is', '$1', $headers );
		$options = array_flip( preg_split( '/[, ]+/', $options_header ));
		return $options;
	}



	/**
	 * Send an XML request to the server (e.g. PROPFIND, REPORT, MKCALENDAR)
	 *
	 * @param string $method The method (PROPFIND, REPORT, etc) to use with the request
	 * @param string $xml The XML to send along with the request
	 * @param string $relative_url The URL to make the request to, relative to $base_url
	 *
	 * @return array An array of the allowed methods
	 */
	function DoXMLRequest($request_method, $xml, $relative_url = '')
	{
		$this->body = $xml;
		$this->requestMethod = $request_method;
		$this->SetContentType('text/xml');
		return $this->DoRequest($relative_url);
	}



	/**
	 * Get a single item from the server.
	 *
	 * @param string $relative_url The part of the URL after the calendar
	 */
	function DoGETRequest( $relative_url ) {
		$this->body = "";
		$this->requestMethod = "GET";
		return $this->DoRequest( $relative_url );
	}


	/**
	 * PUT a text/icalendar resource, returning the etag
	 *
	 * @param string $relative_url The URL to make the request to, relative to $base_url
	 * @param string $icalendar The iCalendar resource to send to the server
	 * @param string $etag The etag of an existing resource to be overwritten, or '*' for a new resource.
	 *
	 * @return string The content of the response from the server
	 */
	function DoPUTRequest( $relative_url, $icalendar, $etag = null ) {
		$this->body = $icalendar;

		$this->requestMethod = "PUT";
		if ( $etag != null ) {
			$this->SetMatch( ($etag != '*'), $etag );
		}
		$this->SetContentType("text/calendar"); //; charset=utf-8");
		$headers = $this->DoRequest($relative_url);

		/**
		 * RSCDS will always return the real etag on PUT.  Other CalDAV servers may need
		 * more work, but we are assuming we are running against RSCDS in this case.
		 */
		$etag = preg_replace( '/^.*Etag: "?([^"\r\n]+)"?\r?\n.*/is', '$1', $headers );
		return $etag;
	}


	/**
	 * DELETE a text/icalendar resource
	 *
	 * @param string $relative_url The URL to make the request to, relative to $base_url
	 * @param string $etag The etag of an existing resource to be deleted, or '*' for any resource at that URL.
	 *
	 * @return int The HTTP Result Code for the DELETE
	 */
	function DoDELETERequest( $relative_url, $etag = null ) {
		$this->body = "";

		$this->requestMethod = "DELETE";
		if ( $etag != null ) {
			$this->SetMatch( true, $etag );
		}
		$this->DoRequest($relative_url);
		//    return $this->resultcode;
	}


	/**
	 * Given XML for a calendar query, return an array of the events (/todos) in the
	 * response.  Each event in the array will have a 'href', 'etag' and '$response_type'
	 * part, where the 'href' is relative to the calendar and the '$response_type' contains the
	 * definition of the calendar data in iCalendar format.
	 *
	 * @param string $filter XML fragment which is the <filter> element of a calendar-query
	 * @param string $relative_url The URL relative to the base_url specified when the calendar was opened.  Default ''.
	 * @param string $report_type Used as a name for the array element containing the calendar data. @deprecated
	 *
	 * @return array An array of the relative URLs, etags, and events from the server.  Each element of the array will
	 *               be an array with 'href', 'etag' and 'data' elements, corresponding to the URL, the server-supplied
	 *               etag (which only varies when the data changes) and the calendar data in iCalendar format.
	 */
	function DoCalendarQuery( $filter, $relative_url = '') {

		$xml = <<<EOXML
<?xml version="1.0" encoding="utf-8" ?>
<C:calendar-query xmlns:D="DAV:" xmlns:C="urn:ietf:params:xml:ns:caldav">
  <D:prop>
    <C:calendar-data/>
    <D:getetag/>
  </D:prop>
		$filter
</C:calendar-query>
EOXML;


		return $this->DoReport($xml, $relative_url);
	}



	public function DoCalendarMultiget( $href, $relative_url = '') {

		$xml = <<<EOXML
<?xml version="1.0" encoding="utf-8" ?>
<C:calendar-multiget xmlns:D="DAV:" xmlns:C="urn:ietf:params:xml:ns:caldav">
  <D:prop>
    <D:getetag/>
    <C:calendar-data/>
  </D:prop>
		$href
</C:calendar-multiget>
EOXML;


		return $this->DoReport($xml, $relative_url);
	}





	/**
	 * Stores the href of the event ics file associated to the specified event UID.
	 *
	 * @param string $uid
	 * @param string $href
	 */
	public function setHrefForUid($uid, $href)
	{
		$key = $this->user .'@'.$this->server.':'.$this->port.$this->base_url.'/'.$uid;
		
		$_SESSION['caldav_href_for_uid_cache'][$key] = $href;
	}





	/**
	 * Returns the event ics file href associated to the specified event UID.
	 *
	 * @param string $uid
	 * @return string or null if no href associated to uid.
	 */
	public function getHrefForUid($uid)
	{
		$key = $this->user .'@'.$this->server.':'.$this->port.$this->base_url.'/'.$uid;
		
		if (!isset($_SESSION['caldav_href_for_uid_cache'][$key])) {
			// default value for href, does not work if the event is not created by LibCaldav!
			return $this->base_url.$uid.'.ics';
		}
		return $_SESSION['caldav_href_for_uid_cache'][$key];
	}




	/**
	 *
	 * @param string $xml
	 * @param xtring $relative_url
	 * @return multitype:multitype:
	 */
	private function DoReport($xml, $relative_url)
	{
		
		$this->DoXMLRequest( 'REPORT', $xml, $relative_url );

		$xml_parser = xml_parser_create_ns('UTF-8');
		$this->xml_tags = array();
		xml_parser_set_option ( $xml_parser, XML_OPTION_SKIP_WHITE, 1 );

		xml_parse_into_struct( $xml_parser, $this->xmlResponse, $this->xml_tags );
		xml_parser_free($xml_parser);

		$report = array();
		foreach( $this->xml_tags as $k => $v ) {
			switch( $v['tag'] ) {
				case 'DAV::RESPONSE':
					if ( $v['type'] == 'open' ) {
						$response = array();
					}
					elseif ( $v['type'] == 'close' ) {
						$report[] = $response;
						
						if (!isset($response['data'])) {
						    bab_debug("CALDAV response without data : \n".bab_toHtml($this->xmlResponse, BAB_HTML_ALL));
						    continue;
						}

					}
					break;
				case 'DAV::HREF':
					$response['href'] = basename( $v['value'] );
					$response['hrefpath'] = $v['value'];
					break;
				case 'DAV::GETETAG':
					$response['etag'] = preg_replace('/^"?([^"]+)"?/', '$1', $v['value']);
					break;
				case 'URN:IETF:PARAMS:XML:NS:CALDAV:CALENDAR-DATA':
					$response['data'] = $v['value'];
					break;
			}
		}

		return $report;
	}



	/**
	 * Get the events in a range from $start to $finish.  The dates should be in the
	 * format yyyymmddThhmmssZ and should be in GMT.  The events are returned as an
	 * array of event arrays.  Each event array will have a 'href', 'etag' and 'event'
	 * part, where the 'href' is relative to the calendar and the event contains the
	 * definition of the event in iCalendar format.
	 *
	 * @param timestamp $start The start time for the period
	 * @param timestamp $finish The finish time for the period
	 * @param string    $relative_url The URL relative to the base_url specified when the calendar was opened.  Default ''.
	 * @param array	 $properties	An array ('name' =>, 'value' => ) of properties to use as a filter.
	 *
	 * @return array An array of the relative URLs, etags, and events, returned from DoCalendarQuery() @see DoCalendarQuery()
	 */
	function GetEvents( $start = null, $finish = null, $relative_url = '', $properties = array())
	{
		$filter = "";

		$prop = '';
		foreach ($properties as $property) {
			$prop .= '<C:prop-filter name="' . $property['name'] . '">' . "\n";
			$prop .= '<C:text-match>' . $property['value'] . '</C:text-match>' . "\n";
			$prop .= '</C:prop-filter>' . "\n";
		}

		if ( isset($start) && isset($finish) ) {
			$range = "<C:time-range start=\"$start\" end=\"$finish\"/>";
		} else {
			$range = '';
		}

		$filter = <<<EOFILTER
  <C:filter>
    <C:comp-filter name="VCALENDAR">
      <C:comp-filter name="VEVENT">
		$range
		$prop
      </C:comp-filter>
    </C:comp-filter>
  </C:filter>
EOFILTER;



		return $this->DoCalendarQuery($filter, $relative_url);
	}


	/**
	 * Get the todo's in a range from $start to $finish.  The dates should be in the
	 * format yyyymmddThhmmssZ and should be in GMT.  The events are returned as an
	 * array of event arrays.  Each event array will have a 'href', 'etag' and 'event'
	 * part, where the 'href' is relative to the calendar and the event contains the
	 * definition of the event in iCalendar format.
	 *
	 * @param timestamp $start The start time for the period
	 * @param timestamp $finish The finish time for the period
	 * @param boolean   $completed Whether to include completed tasks
	 * @param boolean   $cancelled Whether to include cancelled tasks
	 * @param string    $relative_url The URL relative to the base_url specified when the calendar was opened.  Default ''.
	 *
	 * @return array An array of the relative URLs, etags, and events, returned from DoCalendarQuery() @see DoCalendarQuery()
	 */
	function GetTodos( $start, $finish, $completed = false, $cancelled = false, $relative_url = "" ) {

		if ( $start && $finish ) {
			$time_range = <<<EOTIME
                <C:time-range start="$start" end="$finish"/>
EOTIME;
		}

		// Warning!  May contain traces of double negatives...
		$neg_cancelled = ( $cancelled === true ? "no" : "yes" );
		$neg_completed = ( $cancelled === true ? "no" : "yes" );

		$filter = <<<EOFILTER
  <C:filter>
    <C:comp-filter name="VCALENDAR">
          <C:comp-filter name="VTODO">
                <C:prop-filter name="STATUS">
                        <C:text-match negate-condition="$neg_completed">COMPLETED</C:text-match>
                </C:prop-filter>
                <C:prop-filter name="STATUS">
                        <C:text-match negate-condition="$neg_cancelled">CANCELLED</C:text-match>
                </C:prop-filter>$time_range
          </C:comp-filter>
    </C:comp-filter>
  </C:filter>
EOFILTER;

		return $this->DoCalendarQuery($filter, $relative_url);
	}














	/**
	 * Get the calendar entry by UID
	 *
	 * @param string 	uid
	 * @param string    $relative_url The URL relative to the base_url specified when the calendar was opened.  Default ''.
	 *
	 * @return array An array of the relative URL, etag, and calendar data returned from DoCalendarQuery() @see DoCalendarQuery()
	 */
	function GetEntryByUid( $uid, $relative_url = '' )
	{
		$href = $this->getHrefForUid($uid);
		if (isset($href)) {

	        // This is a different way to get an entry by uid.
	        // The standard way does not work with some caldav servers.
	        $href = '<D:href>' . $href . '</D:href>' . "\n";

	        return $this->DoCalendarMultiget($href, $relative_url);

	    } else {

    		$filter = "";
    		if ( $uid ) {
    			$filter = <<<EOFILTER
  <C:filter>
    <C:comp-filter name="VCALENDAR">
          <C:comp-filter name="VEVENT">
                <C:prop-filter name="UID">
                        <C:text-match>$uid</C:text-match>
                </C:prop-filter>
          </C:comp-filter>
    </C:comp-filter>
  </C:filter>
EOFILTER;
    		}

		    return $this->DoCalendarQuery($filter, $relative_url);
	    }

	}



	/**
	 * Get the calendar entries by UIDs
	 *
	 * @param array 	$uid_list
	 * @param string    $relative_url The URL relative to the base_url specified when the calendar was opened.  Default ''.
	 *
	 * @return array An array of the relative URL, etag, and calendar data returned from DoCalendarQuery() @see DoCalendarQuery()
	 */
	function GetEntriesByUid(Array $uid_list, $relative_url = '')
	{
		$hrefList = '';

		if (count($uid_list) > 0) {

			foreach($uid_list as $uid) {
				$href = $this->getHrefForUid($uid);
				if (isset($href)) {
					$hrefList .= '<D:href>' . $href . '</D:href>' . "\n";
				}
			}
		}

		return $this->DoCalendarMultiget($hrefList, $relative_url);
	}


	/**
	 * Get the calendar entry by HREF
	 *
	 * @param string    $href         The href from a call to GetEvents or GetTodos etc.
	 * @param string    $relative_url The URL relative to the base_url specified when the calendar was opened.  Default ''.
	 *
	 * @return string The iCalendar of the calendar entry
	 */
	function GetEntryByHref($href, $relative_url = '')
	{
		return $this->DoGETRequest($relative_url . $href);
	}

}

