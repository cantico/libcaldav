<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2010 by CANTICO ({@link http://www.cantico.fr})
 */

/* @var $inheritedBackend Func_CalendarBackend_Ovi */
$inheritedBackend = bab_functionality::get('CalendarBackend/Ovi');
$inheritedBackend->includeCalendarPeriod();
$inheritedBackend->includeCalendarAlarm();

bab_functionality::get('CalendarBackend/Caldav');


class caldav_CalendarPeriod extends bab_CalendarPeriod
{
	protected $etag = null;


	/**
	 * Returns the etag associated to the period.
	 *
	 * @return string	Or null if no etag associated
	 */
	public function getEtag()
	{
		return $this->etag;
	}

	/**
	 * Sets the etag associated to the period.
	 *
	 * @param string	$etag
	 */
	public function setEtag($etag)
	{
		$this->etag = $etag;
		return $this;
	}


	/**
	 *
	 * @param string $property
	 * @param string $key
	 * @return string or null if not found
	 */
	private function getAttributeFromProperty($property, $key, $valueseparator = ':')
	{
		$list = explode(';', $property);
		foreach($list as $param)
		{
			$colonPos = mb_strpos($param, $valueseparator);
			if (false !== $colonPos)
			{
				if ($key === strtoupper(substr($param, 0, $colonPos)))
				{
					return substr($param, $colonPos + 1);
				}
			}
		}

		return null;
	}


	/**
	 * get author user id from organizer
	 * @see utilit/bab_CalendarPeriod#getAuthorId()
	 */
	public function getAuthorId()
	{
		$organizer = $this->getOrganizer();

		if (!$organizer || !is_array($organizer))
		{
			return null;
		}

		return bab_getUserIdByEmailAndName($organizer['email'], $organizer['name']);
	}



	static private function getPropertyValue($name, $value)
	{
		if ($name === 'DESCRIPTION') {
			// From RFC 2445 :  \\ encodes \, \N or \n encodes newline,  \; encodes ;, \, encodes ,
			$value = str_replace('\\', '\\\\', $value);
			$value = str_replace(';', '\;', $value);
			$value = str_replace(',', '\,', $value);
			$value = str_replace("\n", '\n', $value);
			$value = str_replace("\r", '', $value);
		}
		if ($name === 'DTSTART' || $name === 'DTEND' || $name === 'RDATE' || $name === 'EXDATE' || $name === 'RECURRENCE-ID;VALUE=DATE-TIME') {
		    $name .= ';TZID=' . Func_CalendarBackend_Caldav::getTimezoneName();
		}
		$icalProperty = $name . ':' . $value;

		// From RFC 2445 : Unfolding is accomplished by removing the CRLF character
		// and the linear white space character that immediately follows.
		//
		// When parsing a content line, folded lines MUST first be unfolded
		// according to the unfolding procedure described above. When generating
		// a content line, lines longer than 75 octets SHOULD be folded
		// according to the folding procedure described above.

		if (($length = mb_strlen($icalProperty)) > 75) {
			$property = mb_substr($icalProperty, 0, 75);
			$pos = 75;
			while ($pos < $length) {
				$property .= "\r\n " . mb_substr($icalProperty, $pos, 75);
				$pos += 75;
			}
			$icalProperty = $property;
		}

		return $icalProperty;
	}


	/**
	 * Convert to iCal string
	 * @param bab_CalendarPeriod $period
	 * @return string
	 */
	static public function toIcal(bab_CalendarPeriod $period)
	{
		$ical =  'BEGIN:VEVENT' . "\r\n";

		$priority1 = '';
		$priority2 = '';

		$p1 = array(
			'UID' => 1,
			'DTSTART' => 1,
			'DTEND' => 1,
			'SUMMARY' => 1,
			'DESCRIPTION' => 1
		);

		foreach ($period->getProperties() as $icalProperty) {

			$colonPos = mb_strpos($icalProperty, ':');
			$iCalPropertyName = substr($icalProperty, 0, $colonPos);
			$iCalPropertyValue = substr($icalProperty, $colonPos + 1);
			list($icalPropertyStart) = explode(';', $iCalPropertyName);

			if (trim($iCalPropertyValue) !== '') {

				if (isset($p1[$icalPropertyStart]))
				{
					$priority1 .= self::getPropertyValue($iCalPropertyName, $iCalPropertyValue) . "\r\n";
				} else {
					$priority2 .= self::getPropertyValue($iCalPropertyName, $iCalPropertyValue) . "\r\n";
				}
			}
		}

		$ical .= $priority1;
		$ical .= $priority2;

		if ($alarm = $period->getAlarm()) {
			$ical .= caldav_CalendarAlarm::toIcal($alarm) . "\r\n";
		}

		$ical .= 'END:VEVENT';


		$ical = bab_convertStringFromDatabase($ical, bab_charset::UTF_8);

		return $ical;
	}
	
	




	/**
	 * Initializes the caldav_CalendarPeriod from a icalendar-formatted string.
	 *
	 * @param string	$ical		The icalendar-formatted string
	 * @param string	$encoding	The encoding of the icalendar-formatted string (default 'UTF-8')
	 */
	public function fromIcal($ical, $encoding = bab_charset::UTF_8)
	{
		require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';

		$start = null;
		$end = null;
		$duration = null;


		// Converts the (by default) UTF-8 string into encoding used in ovidentia (eg. ISO-8859-15).
		$ical = bab_getStringAccordingToDataBase($ical, $encoding);


		// From RFC 2445 : Unfolding is accomplished by removing the CRLF character
		// and the linear white space character that immediately follows.
		//
		// When parsing a content line, folded lines MUST first be unfolded
		// according to the unfolding procedure described above. When generating
		// a content line, lines longer than 75 octets SHOULD be folded
		// according to the folding procedure described above.
		$ical = str_replace("\n ", '', $ical);


		// First we extract alarms.
		$alarmsData = null;
		preg_match_all('/BEGIN:VALARM(.*)END:VALARM/Us', $ical, $alarmsData);

		foreach ($alarmsData[1] as $alarmData) {
			$alarm = new caldav_CalendarAlarm();
			$alarm->fromIcal($alarmData);
			$this->setAlarm($alarm);

			// For now, ovidentia only supports one alarm by event.
			break;
		}

		$ical = preg_replace('/BEGIN:VALARM.*END:VALARM/Us', '', $ical);


		foreach (explode("\n", $ical) as $dataLine) {
			if (empty($dataLine) || strstr($dataLine, ':') === false) {
				continue;
			}
			
/*
 
$dataLine example :

ORGANIZER;SENT-BY="mailto:nicolas.bidault@ubuntu.cantico.fr":mailto:clio@ubu
 ntu.cantico.fr
 
 
ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=Dupont Robert:MAILTO:paul.derosanbo@cantico.fr
 
*/		
			
			
			
			$m = null;
			if (preg_match('/^([^:^;]+)/', $dataLine, $m))
			{
				$icalPropertyStart = $m[1];
			}
			
			$dataLine = substr($dataLine, strlen($icalPropertyStart));
			
			$m = null;
			if (preg_match('/^;(.+)$/', $dataLine, $m))
			{
				$parameters = explode(';', $m[1]);
				
			
				foreach($parameters as $key => $p)
				{
				    $mp = null;
					if (preg_match('/^[^=]+=(?:(?:[^"][^:]+)|(?:"[^"]+"))/', $p, $mp))
					{
						$parameters[$key] = $mp[0];
						$iCalPropertyValue = substr($p, (1 + strlen($mp[0])));
						
					}
				}
				
				$iCalPropertyName = $icalPropertyStart.';'.implode(';',$parameters);
			
			} else {
			
				$colonPos = mb_strpos($dataLine, ':');
				$iCalPropertyName = $icalPropertyStart;
				$iCalPropertyValue = substr($dataLine, $colonPos + 1);
			}
			

			// From RFC 2445 :  \\ encodes \, \N or \n encodes newline,  \; encodes ;, \, encodes ,
			$iCalPropertyValue = str_replace('\n', "\n", $iCalPropertyValue);
			$iCalPropertyValue = str_replace('\N', "\n", $iCalPropertyValue);
			$iCalPropertyValue = str_replace('\\\\', '\\', $iCalPropertyValue);
			$iCalPropertyValue = str_replace('\;', ';', $iCalPropertyValue);
			$iCalPropertyValue = str_replace('\,', ',', $iCalPropertyValue);


			switch ($icalPropertyStart) {

				case 'DESCRIPTION':
					//					var_dump(bab_toHtml($iCalPropertyValue, BAB_HTML_ALL));
					$this->setData(array('description' => bab_toHtml($iCalPropertyValue, BAB_HTML_ALL), 'description_format' => 'html'));
					$this->setProperty($iCalPropertyName, $iCalPropertyValue);
					break;

				case 'DTSTART':
					list($iCalPropertyName) = explode(';', $iCalPropertyName);
					$start = BAB_DateTime::fromICal($iCalPropertyValue);
					$this->setProperty($iCalPropertyName, $iCalPropertyValue);
					break;

				case 'DTEND':
					list($iCalPropertyName) = explode(';', $iCalPropertyName);
					$end = BAB_DateTime::fromICal($iCalPropertyValue);
					$this->setProperty($iCalPropertyName, $iCalPropertyValue);
					break;

				case 'DURATION':
					list($iCalPropertyName) = explode(';', $iCalPropertyName);
					$duration = $iCalPropertyValue;
					$this->setProperty($iCalPropertyName, $iCalPropertyValue);
					break;

				case 'ATTENDEE':
				    list($role, $partstat, $cn, $email) = caldav_getAttendeeProp($iCalPropertyName, $iCalPropertyValue);
					

					if ($userId = bab_getUserIdByEmailAndName($email, $cn)) {
						
						if (null === $userId)
						{
							$userId = ''; // default
						}
						
						$calendars = bab_getICalendars($userId);
						$reftype = null;
						if ($id_calendar = $calendars->getPersonalCalendarUid($userId))
						{
							$reftype = $calendars->getUserReferenceType($userId);
						}
						
						
						if (null !== $id_calendar && null !== $reftype)
						{
						
							$usercal = $calendars->getEventCalendar("$reftype/$id_calendar");								

							if (isset($usercal))
							{
								try {
									$this->addAttendee($usercal, $role, $partstat);
								} catch(Exception $e) {
									bab_debug($e->getMessage());
								}
							} else {
								bab_debug('missing calendar '."$reftype/$id_calendar");
							}
						}
						
					} else {
						$this->setProperty($iCalPropertyName, $iCalPropertyValue);
					}

					break;

				case 'RELATED-TO':
					$parameters = explode(';', $iCalPropertyName);
					array_shift($parameters);

					$reltype = null;
					$status = null;
					$wfinstance = null;

					foreach ($parameters as $parameter) {
						list($paramName, $paramValue) = explode('=', $parameter);

						switch ($paramName) {
							case 'RELTYPE':
								$reltype = $paramValue;
								break;

							case 'X-CTO-STATUS':
								$status = $paramValue;
								break;

							case 'X-CTO-WFINSTANCE':
								$wfinstance = (int) $paramValue;
								break;
						}
					}
					if (!empty($reltype)) {
						require_once $GLOBALS['babInstallPath'] . 'utilit/reference.class.php';
						try {
							$ref = new bab_Reference($iCalPropertyValue);

							$referencedCalendar = bab_getICalendars()->getEventCalendar($ref->getType().'/'.$ref->getObjectId());
							if ($referencedCalendar) {
								$this->addRelation($reltype, $referencedCalendar, $status, $wfinstance);
							}
						} catch (Exception $e) {
						}
					}
					break;


				default:
					$this->setProperty($iCalPropertyName, $iCalPropertyValue);
					break;

			}

		}
		if (!isset($end)) {
		    if (isset($duration) && isset($start)) {
				$end = $start->cloneDate();
				$end->add($duration, BAB_DATETIME_ICAL);
			} else {
				throw new Exception('Missing End date or duration in calendar event '.$ical);
			}
		}

		if (isset($start) && isset($end))
		{
			$this->setDates($start, $end);
		}


		// Kerio specific:
		// when events selected from KERIO caldav server,
		// the ORGANIZER of an event is removed from the list of attendees
		// to fix this problem, we add X-CTO-ORGANIZER-PARTSTAT to store the PARTSTAT of the ORGANIZER
		// only if the organizer is in the list of attendees

		if ($partstat = $this->getProperty('X-CTO-ORGANIZER-PARTSTAT'))
		{
			$id_author = $this->getAuthorId();
			
			if (empty($id_author))
			{
				bab_debug(sprintf('Author not found in ovidentia database for event %s in the ORGANIZER property', $this->getProperty('UID')));
				
			} else {
			
				$refType = bab_getICalendars()->getUserReferenceType($id_author);
				if ($refType)
				{
					$id_calendar = bab_getICalendars()->getPersonalCalendarUid($id_author);
					$calendar = bab_getICalendars()->getEventCalendar("$refType/$id_calendar");
	
					if ($calendar)
					{
						$this->addAttendee($calendar, null, $partstat);
					} else {
						throw new Exception('Calendar not found for organizer '.$id_author);
					}
				} else {
					// the organizer has no accessible calendar
					$this->addAttendeeByUserId($id_author, 'REQ-PARTICIPANT', $partstat);
				}
			}
		}
	}



	/**
	 * define a property with a icalendar property name
	 * 
	 *
	 * @param	string	$icalProperty
	 * @param	mixed	$value
	 *
	 * @return caldav_CalendarPeriod
	 */
	public function setProperty($icalProperty, $value) {
		/*
		if ((substr($icalProperty, 0, strlen('ATTENDEE')) === 'ATTENDEE')
		 || (substr($icalProperty, 0, strlen('RELATED-TO')) === 'RELATED-TO')) {
			return $this;
		}
		*/
		return parent::setProperty($icalProperty, $value);
	}

}


/**
 *
 */
class caldav_CalendarAlarm extends bab_CalendarAlarm
{
	static public function toIcal(bab_CalendarAlarm $alarm)
	{
		$ical =  'BEGIN:VALARM' . "\r\n";

		foreach ($alarm->getProperties() as $icalProperty) {
			$colonPos = mb_strpos($icalProperty, ':');
			$iCalPropertyName = substr($icalProperty, 0, $colonPos);
			$iCalPropertyValue = substr($icalProperty, $colonPos + 1);
			list($icalPropertyStart) = explode(';', $iCalPropertyName);

			//			var_dump($icalProperty);
			if (trim($iCalPropertyValue) !== '') {
				if ($icalPropertyStart === 'DESCRIPTION') {
					// From RFC 2445 :  \\ encodes \, \N or \n encodes newline,  \; encodes ;, \, encodes ,
					$iCalPropertyValue = str_replace('\\', '\\\\', $iCalPropertyValue);
					$iCalPropertyValue = str_replace(';', '\;', $iCalPropertyValue);
					$iCalPropertyValue = str_replace(',', '\,', $iCalPropertyValue);
					$iCalPropertyValue = str_replace("\n", '\n', $iCalPropertyValue);
					$iCalPropertyValue = str_replace("\r", '', $iCalPropertyValue);
				}

				$icalProperty = $iCalPropertyName . ':' . $iCalPropertyValue;

				// From RFC 2445 : Unfolding is accomplished by removing the CRLF character
				// and the linear white space character that immediately follows.
				//
				// When parsing a content line, folded lines MUST first be unfolded
				// according to the unfolding procedure described above. When generating
				// a content line, lines longer than 75 octets SHOULD be folded
				// according to the folding procedure described above.
				if (($length = mb_strlen($icalProperty)) > 75) {
					$property = mb_substr($icalProperty, 0, 75);
					$pos = 75;
					while ($pos < $length) {
						$property .= "\n " . mb_substr($icalProperty, $pos, 75);
						$pos += 75;
					}
					$icalProperty = $property;
				}
				$ical .= $icalProperty . "\r\n";
			}
		}

		$ical .= 'END:VALARM';

		$ical = iconv(bab_charset::getDatabase(), bab_charset::UTF_8, $ical);

		return $ical;
	}

	/**
	 * Initializes the caldav_CalendarPeriod from a icalendar-formatted string.
	 *
	 * @param string	$ical		The icalendar-formatted string
	 * @param string	$encoding	The encoding of the icalendar-formatted string (default 'UTF-8')
	 */
	public function fromIcal($ical, $encoding = bab_charset::UTF_8)
	{
		foreach (explode("\n", $ical) as $dataLine) {
			if (empty($dataLine) || strstr($dataLine, ':') === false) {
				continue;
			}
			//			list($iCalPropertyName, $iCalPropertyValue) = explode(':', $dataLine);//
			$colonPos = mb_strpos($dataLine, ':');
			$iCalPropertyName = substr($dataLine, 0, $colonPos);
			$iCalPropertyValue = substr($dataLine, $colonPos + 1);
			list($icalPropertyStart) = explode(';', $iCalPropertyName);

			// From RFC 2445 :  \\ encodes \, \N or \n encodes newline,  \; encodes ;, \, encodes ,
			$iCalPropertyValue = str_replace('\n', "\n", $iCalPropertyValue);
			$iCalPropertyValue = str_replace('\N', "\n", $iCalPropertyValue);
			$iCalPropertyValue = str_replace('\\\\', '\\', $iCalPropertyValue);
			$iCalPropertyValue = str_replace('\;', ';', $iCalPropertyValue);
			$iCalPropertyValue = str_replace('\,', ',', $iCalPropertyValue);


			//			$iCalPropertyValue = iconv(bab_charset::UTF_8, bab_charset::getDatabase(), $iCalPropertyValue);



			switch ($icalPropertyStart) {

				case 'DESCRIPTION':
					//					var_dump(bab_toHtml($iCalPropertyValue, BAB_HTML_ALL));
//					$this->setData(array('description' => bab_toHtml($iCalPropertyValue, BAB_HTML_ALL), 'description_format' => 'html'));
					$this->setProperty($iCalPropertyName, $iCalPropertyValue);
					break;

				case 'ATTENDEE':
				    list($role, $partstat, $cn, $email) = caldav_getAttendeeProp($iCalPropertyName, $iCalPropertyValue);
					
					if ($userId = bab_getUserIdByEmailAndName($email, $cn)) {
						$calendars = bab_getICalendars()->getCalendars();
						foreach ($calendars as $cal) {
							if ($cal->getIdUser() == $userId) {
								try {
									$this->addAttendee($cal, $role, $partstat);
								} catch(Exception $e) {
									//								echo $e;
								}
								break;
							}
						}
					} else {
						$this->setProperty($iCalPropertyName, $iCalPropertyValue);
					}
					
					break;

				default:
					$this->setProperty($iCalPropertyName, $iCalPropertyValue);
					break;

			}

		}

	}
}