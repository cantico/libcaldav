<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2010 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'] . 'utilit/cal.eventcalendar.class.php';


/**
 * Ovidentia calendar
 */
abstract class caldav_EventCalendar extends bab_EventCalendar
{

	/**
	 * The backend that was used to create this calendar instance.
	 * @var Func_CalendarBackend
	 */
	protected $backend;

	/**
	 * Calendar ID
	 * @var string
	 */
	protected $id_calendar = null;
	
	/**
	 * @param Func_CalendarBackend $backend
	 */
	public function __construct(Func_CalendarBackend $backend)
	{
		$this->uid = null;
		$this->backend = $backend;
	}
	
	
	/**
	 * @return CalDAVClient
	 */
	abstract function getCaldavClient();
}




/**
 * Personal calendar
 */
class caldav_PersonalCalendar extends caldav_EventCalendar implements bab_PersonalCalendar 
{


	/**
	 * Sets the name of the calendar.
	 * 
	 * @param string $name
	 * 
	 * @return caldav_PersonalCalendar
	 */
	public function setName($name)
	{
		$this->name = $name;
		
		return $this;
	}

	/**
	 * (non-PHPdoc)
	 * @see utilit/bab_EventCalendar::getBackend()
	 * 
	 * @return Func_CalendarBackend_Caldav
	 */
	public function getBackend()
	{
		return $this->backend;
	}


	/**
	 * Sets the user associated to this personal calendar.
	 * 
	 * @param int $userId
	 * 
	 * @return caldav_PersonalCalendar
	 */
	public function setIdUser($userId)
	{
		$this->id_user = $userId;
		$this->name = bab_getUserName($userId);
		
		$this->uid = bab_getICalendars()->getPersonalCalendarUid($userId);
		if (!$this->uid)
		{
			throw new Exception('Missing calendar UID');
		}
		
		return $this;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see utilit/bab_EventCalendar::getType()
	 */
	public function getType() 
	{
		return bab_translate('Personal calendar');
	}


	/**
	 * Get the type part of the reference
	 * @return string
	 */
	public function getReferenceType()
	{
		return 'caldav_personal';
	}
	

	
	/**
	 * Test if an event can be added on a calendar
	 * @return bool
	 */
	public function canAddEvent()
	{
		
		if (((int) $this->access_user) === (int) $this->getIdUser()) {
			// i am the author
			return true;
		}
		
		
		switch($this->getSharingAccess()) {
			case BAB_CAL_ACCESS_SHARED_UPDATE:
			case BAB_CAL_ACCESS_UPDATE:
			case BAB_CAL_ACCESS_FULL:
				return true;
		}

		return false;
	}


	/**
	 * Test if the current user can update the specified event of the calendar.
	 * 
	 * @param bab_calendarPeriod $event
	 * @return bool
	 */
	public function canUpdateEvent(bab_CalendarPeriod $event)
	{	
		$collection = $event->getCollection();

		if ($collection instanceof bab_ReadOnlyCollection) {
			return false;
		}
		
		$author = $event->getAuthorId();
		if (null === $author)
		{
			bab_debug('Missing author ID for event '.$event->getUrlIdentifier());
			return (((int) $this->getIdUser()) === (int) $this->access_user);
		}
		
		


		if (((int) $this->access_user) === $author) {
			// i am the author
			return true;
		}

		if ($event->isLocked()) {
			return false;
		}


		switch($this->getSharingAccess()) {
				
			case BAB_CAL_ACCESS_UPDATE:
				if (((int) $this->access_user) === $author)
				{
					return true;
				}
				break;

			case BAB_CAL_ACCESS_SHARED_UPDATE:
				if ($this->isSharedAccessForCalendar($this, $event))
				{
					return true;
				}
				break;

			case BAB_CAL_ACCESS_FULL:
				return true;
		}
		
		
		
		$main = $event->getCollection()->getCalendar();
		if ($main === $this)
		{
			return true;
		}
		
		
		return false;
	}


	/**
	 * Test if the current user can delete the specified event from the calendar.
	 * 
	 * @param bab_calendarPeriod $event
	 * @return bool
	 */
	public function canDeleteEvent(bab_CalendarPeriod $event)
	{
		return $this->canUpdateEvent($event);
	}

	/**
	 * Access level for calendar sharing of the access_user
	 */
	public function getSharingAccess()
	{
		return $this->getSharingAccessForCalendar($this);
	}


	/**
	 * Get default attendee PARTSTAT property value for new attendee associated to an event of this calendar
	 * The calendar as given parameter must return an interger value with the method getIdUser
	 * the return value will be one of the following values from the iCalendar spec :
	 * <ul>
	 * 	<li>NEEDS-ACTION : the event will appear on the attendee calendar and request validation from him (default value)</li>
	 *  <li>ACCEPTED : the event will appear on the attendee calendar</li>
	 * </ul>
	 * if the user is the attendee or if the user have full access, the attendee is considered accepted
	 * 
	 * @link http://www.kanzaki.com/docs/ical/partstat.html
	 * 
	 * @see bab_EventCalendar::getIdUser()
	 * 
	 * 
	 * @return 	string
	 */
	public function getDefaultAttendeePARTSTAT()
	{
		
		if ($this->access_user == $this->getIdUser())
		{
			// I add myself as attendee on an event
			return 'ACCEPTED';
		}
		
//		switch($this->getSharingAccess()) {
//
//			case BAB_CAL_ACCESS_FULL:
//				// i have full access on the attendee calendar where the event is
//				return 'ACCEPTED';
//		}

		return 'NEEDS-ACTION';
	}
	
	
	/**
	 * 
	 *
	 *
	 */
	protected function inviteTo(bab_ICalendarObject $icalendarObject)
	{
		$mail = bab_mail();
		if ($mail === false) {
			throw new Exception('Sending error( Mail sending disabled )');
		}
		
		$senderId = $GLOBALS['BAB_SESS_USERID'];
		$senderEmailAddress = bab_getUserEmail($senderId);
		$senderFullname = bab_getUserName($senderId, true);
		 
		$mail->mailFrom($senderEmailAddress, $senderFullname);
//		$mail->mailBody('');
		$mail->mailSubject($icalendarObject->getProperty('SUMMARY'));
		
		$userId = $this->getIdUser();
		$userEmailAddress = bab_getUserEmail($userId);
		$userFullname = bab_getUserName($userId, true);
		$mail->mailStringAttach($this->getBackend()->iCalObjectToVcalendarInvitation($icalendarObject, $this), $icalendarObject->getProperty('UID') . '.ics', 'text/calendar');
		$mail->mailTo($userEmailAddress, $userFullname);
		
		if (!$mail->send()) {
			throw new Exception('Error occured when sending email: ' . $mail->ErrorInfo());
		}
		
	}
	
	
	
	
	/**
	 * save or update an event
	 * this method is called on the attendee calendar of the event
	 * 
	 * @param	bab_CalendarPeriod 	$event
	 */
	protected function saveToCaldavInbox(bab_CalendarPeriod $event)
	{
		$collection = $event->getCollection();
		
		if (!($collection instanceof bab_CalendarEventCollection))
		{
			// do nothing because the event is not a regular calendar event
			return;	
		}
		
		$backend = $this->getBackend();
		
		$attendeeEvent = clone $event;
		
		// create a new collection into the attendee calendar
		$collection = $backend->CalendarEventCollection($this);
		$collection->addPeriod($attendeeEvent);
		
		// save a copy of the event into the attendee calendar
		// if the event allready exists, it will be updated
		// TODO : save the event into the .in folder if possible
		$backend->savePeriod($attendeeEvent);
	}
	
	
	
	
	
	

	/**
	 * Triggered when the calendar has been added as an attendee on $event
	 * @param bab_CalendarPeriod $event
	 * @return unknown_type
	 */
	public function onAddAttendee(bab_CalendarPeriod $event)
	{
		// do not invite organizer
		
		$collection = $event->getCollection();
		$calendar = $collection->getCalendar();

		if ($calendar === $this)
		{
			return;
		}
		
		// send mail with ics attachment
		// $this->inviteTo($event);
		
		// call parent to add the event in ovidentia inbox
		// event will be displayed but not created in calendar
		// if this line is active, the inviteTo method should not be called 
		// $this->addToOviInbox($event);
		
		
		
		// save the event into the calendar on caldav server
		try {
		    $this->saveToCaldavInbox($event);
		} catch (ErrorException $e) {
		    bab_debug($e->getMessage());
		    bab_getBody()->addError($e->getMessage());
		}
	}
	
	/**
	 * Triggered when the calendar has been updated as an attendee on $event
	 * @param bab_CalendarPeriod $event
	 * @return unknown_type
	 */
	public function onUpdateAttendee(bab_CalendarPeriod $event)
	{
		// notify the user with the event, he will probablay allready have the event with same uid in his calendar 
		// the mail client will propose an event update
		// $this->inviteTo($event);
		
		// update the event into the calendar on caldav server if the event allready exists
		// or create the event
		$this->saveToCaldavInbox($event);
	}
	
	
	
	
	
	/**
	 * (non-PHPdoc)
	 * @see caldav_EventCalendar::getCaldavClient()
	 * 
	 * @return CalDAVClient
	 */
	public function getCaldavClient()
	{
		$userId = $this->getIdUser();
		$backend = $this->getBackend();
		
		$path = $backend->getPersonnalCalendarUrl($userId);
		list($nickname, $password) = $backend->getPersonalCalendarCredentials($userId);
		
		
		
		return new CalDAVClient($path, $nickname, $password, 'calendar');
	}
}




 class caldav_ResourceCalendar extends caldav_EventCalendar implements bab_ResourceCalendar 
 {
 	public function getBackend()
 	{
 		return $this->backend;
 	}
 	
 	
 	/**
 	 * (non-PHPdoc)
 	 * @see utilit/bab_EventCalendar::getType()
 	 */
 	public function getType()
 	{
 		return bab_translate('Resource calendar');
 	}
 	
 	
 	/**
 	 * Get the type part of the reference
 	 * @return string
 	 */
 	public function getReferenceType()
 	{
 		return 'caldav_resource';
 	}
 	
 	
 	/**
 	 * Triggered when the calendar has been added as a relation on $event
 	 * @param bab_CalendarPeriod $event
 	 * @return unknown_type
 	 */
 	public function onAddRelation(bab_CalendarPeriod $event) {}
 	
 	/**
 	 * Triggered when the calendar has been updated as a relation on $event
 	 * @param bab_CalendarPeriod $event
 	 * @return unknown_type
 	 */
 	public function onUpdateRelation(bab_CalendarPeriod $event) {}
 	
 	
 	
 	public function setUid($uid)
 	{
 		$this->uid = $uid;
 	}
 	
 	
 	public function setConfiguration(Array $configuration)
 	{
 		$this->configuration = $configuration;
 		
 	}
 	
 	
 	public function setName($name)
 	{
 		$this->name = $name;
 	}
 	
 	
 	/**
 	 * (non-PHPdoc)
 	 * @see caldav_EventCalendar::getCaldavClient()
 	 *
 	 * @return CalDAVClient
 	 */
 	public function getCaldavClient()
 	{
 		return new CalDAVClient($this->configuration['url'], $this->configuration['nickname'], $this->configuration['password'], 'calendar');
 	}
 }
