<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'] . 'utilit/mcalincl.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
require_once dirname(__FILE__) . '/calendarperiod.class.php';

/**
 * Create events object
 * for all calendars
 *
 * @param	string	$startdate	ISO datetime
 * @param	string	$enddate	ISO datetime
 * @param	array	$idcals		list of url identifier of calendars
 *
 * @return	bab_UserPeriods
 */
function create_events($startdate, $enddate, $idcals)
{
	include_once $GLOBALS['babInstallPath']."utilit/cal.userperiods.class.php";



	$whObj = new bab_UserPeriods(
		BAB_DateTime::fromIsoDateTime($startdate),
		BAB_DateTime::fromIsoDateTime($enddate)
	);

 	$factory = bab_getInstance('bab_PeriodCriteriaFactory');
 	/* @var $factory bab_PeriodCriteriaFactory */

	$criteria = $factory->Collection(
		array(
			'bab_NonWorkingDaysCollection',
		)
	);

 	$whObj->createPeriods($criteria);
 	$whObj->orderBoundaries();


	return $whObj;
}



$now = BAB_DateTime::now();
$start = $now->cloneDate();
$start->add(-2, BAB_DATETIME_YEAR);
$end = $now->cloneDate();
$end->add(2, BAB_DATETIME_YEAR);

header('Content-type: text/calendar; charset=utf-8');
header('Content-Disposition: inline; filename=holidays.ics');


$whObj = create_events($start->getIsoDate(), $end->getIsoDate(), array());
$cal = new bab_icalendarNWorkingDays($whObj);

$events = array();
$cal->getEvents($start->getIsoDate(), $end->getIsoDate(), $events);

echo 'BEGIN:VCALENDAR' . "\r\n";
echo 'VERSION:2.0' . "\r\n";
echo 'PRODID:-//hacksw/handcal//NONSGML v1.0//EN' . "\r\n";
foreach ($events as $event) {
	echo caldav_CalendarPeriod::toIcal($event);
	echo "\r\n";
}

echo 'END:VCALENDAR' . "\r\n";

die;

