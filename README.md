## Connecteur vers les agendas CalDav ##

Caldav library

[![Build Status](https://drone.io/bitbucket.org/cantico/libcaldav/status.png)](https://drone.io/bitbucket.org/cantico/libcaldav/latest)

[![Code Coverage](https://scrutinizer-ci.com/b/cantico/libcaldav/badges/coverage.png?b=master)](https://scrutinizer-ci.com/b/cantico/libcaldav/?branch=master)

Connecteur CalDAV